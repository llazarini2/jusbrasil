/**
 * Versão 1.0.0
 */

var submit = false;

/**
 * Executa set automático de submit
 */
$(function () {
    set_submit();
})

/**
 * Seta submit automático para formulários
 */
function set_submit()
{
    $('form').unbind("submit");
    $('form').submit(function(e)
    {
        submeter_formulario($(this), e);
    });
}


/**
 * Submete formulário para validação
 * @param form_obj
 * @param e
 * @param opts
 * @returns {boolean}
 */
function submeter_formulario(form_obj, e, opts)
{
    var formulario = form_obj;
    var retorno = false;

    if(typeof opts == typeof undefined)
        var opts = {}

    if(typeof opts.redirect == typeof undefined)
        opts.redirect = true;

    if(typeof opts.sucesso == typeof undefined)
        opts.sucesso = function(){};

    if(typeof opts.always == typeof undefined)
        opts.always = function(){};

    if(typeof opts.erro == typeof undefined)
        opts.erro = function(){};

    if(typeof opts.callback == typeof undefined)
        opts.callback = false;

    //Verifica se possui dados
    if(!submit && formulario.attr('validate') != 'false' && typeof formulario.attr('model_validate') != typeof undefined)
    {
        if(typeof e != typeof undefined)
            e.preventDefault();


        //Regata dados da form
        var form_data = {};

        //Retorna array de campos
        $.each(formulario.closest('form').serializeArray(), function(_, campo)
        {
            form_data[campo.name] = campo.value;

            if(typeof $("[name='" + campo.name + "']").data("kendoDropDownList") != typeof undefined)
            {
                form_data[campo.name] = $("[name='" + campo.name + "']").data("kendoDropDownList").value();
            }

        });


        var group = "default";

        if(typeof formulario.attr('group') != typeof undefined)
            group =  formulario.attr('group');


        var model_validate = formulario.attr('model_validate');


        //Executa post e checa se form é válido
        $.ajax({
            type: "POST",
            url: base_api + 'Form_validate/validate/' + model_validate + "/" + group,
            data: form_data,
            dataType: 'json',
            success: function (data)
            {
                if(data.status)
                {
                    var action = formulario.attr("action");

                    var nr = 0;
                    if(typeof formulario.attr("new_record") == typeof undefined)
                        nr = formulario.find("input[name='new_record']").val();
                    else
                        nr = formulario.attr("new_record")

                    if(nr === 1 || nr == true)
                        var act = "add";
                    else
                        var act = "edit";

                    if(typeof action == typeof undefined)
                        action = base_api + "Requisicao/" + act + "/" + formulario.attr("model_validate") + "/" + group;

                    var form_data = new FormData(formulario[0]);

                    //Seta form KENDO
                    $.each(formulario.closest('form').serializeArray(), function(_, campo)
                    {

                        if(typeof $("[name='" + campo.name + "']").data("kendoDropDownList") != typeof undefined)
                        {
                            form_data.append(campo.name, $("[name='" + campo.name + "']").data("kendoDropDownList").value());
                        }

                        /*
                        if(typeof $("select[name='" + campo.name + "']").data("kendoDropDownList") != typeof undefined)
                        {
                            form_data.append(campo.name, $("select[name='" + campo.name + "']").data("kendoDropDownList").value());
                        }*/
                    });


                    var submit_return = $.ajax({
                        type: "POST",
                        url: action,
                        data: form_data,
                        dataType: 'json',
                        async: false,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function(response)
                        {
                            if(response.status)
                            {
                                //toastr.success(response.message, "Sucesso")
                            }
                            else
                            {
                                toastr.error(response.message, "Erro")
                            }

                            if(response.redirect && opts.redirect)
                            {
                                window.location.href = response.redirect;
                            }

                            if(opts.callback)
                            {
                                if(response.status)
                                    opts.sucesso(response);
                                else
                                    opts.erro(response);

                            }

                        }
                    });

                    return submit_return;
                }
                else
                {

                    $("input, select").removeClass("invalid");

                    toastr.clear();

                    var i = 0;
                    $.each(data.data, function(field, message)
                    {
                        toastr.error(message, "Atenção.");

                        $("[name='"+field+"']").addClass("invalid");

                        if(i == 0)
                        {
                            $("[name='"+field+"']").focus();
                        }
                        i++;
                    })

                    if(data.data.lenght == 0 && data.message != '')
                    {
                        toastr.error(data.message, "Atenção.");
                    }

                    return false;

                }
            }
        })
            .always(function ()
            {
                //Se tiver callback, chama always
                if(opts.callback)
                {
                    opts.always();
                }
            });
    }

    if(!opts.redirect)
    {
        return retorno;
    }
}

/**
 * Valida um formulário e chama respectivos callbacks
 * @param form_obj
 * @param opts
 */
function valida_formulario (form_obj, opts)
{
    var valido = false,
        formulario = form_obj;

    if(typeof opts == typeof undefined)
    {
        var opts = {
            valido: function(){},
            invalido: function(){},
        };
    }

    //Regata dados da form
    var form_data = {};

    //Retorna array de campos
    $.each(formulario.closest('form').serializeArray(), function(_, campo)
    {
        form_data[campo.name] = campo.value;
    });

    var group = "default";

    if(typeof formulario.attr('group') != typeof undefined)
        group =  formulario.attr('group');

    var model_validate = formulario.attr('model_validate');

    $.ajax({
        type: "POST",
        url: base_api + 'Form_validate/validate/' + model_validate + "/" + group,
        data: form_data,
        success: function (data)
        {
            var data = JSON.parse(data);

            if(data.status)
                opts.valido(data);
            else
                opts.invalido(data);
        }
    });

    return retorno;
}