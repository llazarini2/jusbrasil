/**
 * Configura o Rivets
 */

rivets.configure({

    // Attribute prefix in templates
    prefix: 'rv',

    // Preload templates with initial data on bind
    preloadData: true,

    // Root sightglass interface for keypaths
    rootInterface: '.',

    // Template delimiters for text bindings
    templateDelimiters: ['{', '}'],

    // Alias for index in rv-each binder
    iterationAlias : function(modelName) {
        return '%' + modelName + '%';
    },

    // Augment the event handler of the on-* binder
    handler: function(target, event, binding) {
        this.call(target, event, binding.view.models)
    },

    // Since rivets 0.9 functions are not automatically executed in expressions. If you need backward compatibilty, set this parameter to true
    executeFunctions: true

})



/**
 * Concatena valores
 * @param model
 * @returns {string}
 */
rivets.formatters.concat = function(model) {
    var str = "";

    for (var i=1; i < arguments.length; i++)
    {
        var str_concat = arguments[i];

        if(typeof str_concat != typeof undefined && str_concat != null)
        {
            if(typeof str_concat.replace != typeof undefined)
            {
                str_concat = str_concat.replace("{base_url}", base_url)
                str_concat = str_concat.replace("{img_src}", base_url + "services/Image/get_image/arquivos/")
            }

            str += str_concat;
        }
        else
            str = "";

    }

    return str;
}

/**
 * Formata uma moeda
 * @param v
 * @returns {*}
 */
rivets.formatters.moeda = function(v){

    if(isNaN(parseFloat(v)))
        return 0;
    
    return parseFloat(v).toFixed(2).replace(".",",");

}

/**
 * Busca em um array
 * @param array
 * @param index
 * @param value
 * @param return_index
 * @returns {*}
 */
function array_search(array, index, value, return_index)
{
    if(typeof return_index == typeof undefined)
        return_index = false;

    var ret = null;
    array.forEach(function(val,i)
    {
        if(index == "index")
        {
            if(i == value.toString())
            {
                if(return_index)
                    ret = i;
                else
                    ret = val;
            }
        }
        else
        {
            if(typeof value != typeof undefined && val[index].toString() == value.toString())
            {
                if(return_index)
                    ret = i;
                else
                    ret = val;
            }
        }

    })
    return ret;
}