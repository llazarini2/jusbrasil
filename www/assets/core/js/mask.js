/**
 * Created by Leonardo on 19/05/2015.
 */
//Máscaras

$(function()
{
    set_masks();
});

/**
 * Seta máscaras
 */
function set_masks()
{
    $(".mask-data").mask("99/99/9999");
    $('.mask-placa').mask('AAA-0000');
    $('.mask-cpf').mask('000.000.000-00');
    $('.mask-cnpj').mask("99.999.999/9999-99");
    $('.mask-tamanho-camiseta').mask('99');
    $('.mask-cep').mask('00000-000');
    $('.mask-money').mask('000.000.000.000.000,00', {reverse: true});
    $('.mask-quilometragem').mask('000.000.000.000.000', {reverse: true});

    $('.mask-telefone-fixo').mask('(00) 0000-0000');
    $('.mask-telefone').mask('(00) 0000-00009');
    $('.mask-telefone').on('input',function()
    {
        if($(this).val().length >= 15)
        {
            $(this).mask('(00) 00000-0000');
        } else {
            $(this).mask('(00) 0000-00009');
        }
    });

}

function valida_cpf(strCPF)
{
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000") return false;

    for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

    Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}

function apenas_numeros(str)
{
    return str.match(/\d/g).join("");;
}


function pad_zero(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}