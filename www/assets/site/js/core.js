
console.log(c)

if(typeof c == typeof undefined)
{
    var c = {
        init: function(){

        }
    };
}


$(function () {

    /* image-carousel no margin function by = owl.carousel.js */
    jQuery('.owl-slider').owlCarousel({
        loop:true,
        autoplay:true,
        margin:0,
        nav:false,
        dots: true,
        navText: ['<i class="flaticon-left-arrow"></i>', '<i class="flaticon-right-arrow"></i>'],
        responsive:{
            0:{
                items:1
            },
            480:{
                items:1
            },
            1024:{
                items:1
            },
            1200:{
                items:1
            }
        }
    })

    $("#envia_email").click(function(){

        if($("#contato_email").val() != "" && $("#contato_nome").val() != "" && $("#contato_mensagem").val() != "")
        {
            $.ajax({
                url: base_url + "services/Email/enviar_email/contato_site",
                type: "POST",
                data: {
                    to: $("#contato_email").val(),
                    nome: $("#contato_nome").val(),
                    mensagem: $("#contato_mensagem").val(),
                },
                dataType: "JSON",
                success: function(response)
                {
                    if(response.status)
                    {
                        toastr.success("Sucesso ao enviar e-mail.")
                        $("#contato_email, #contato_nome, #contato_mensagem").val("");
                    }
                    else
                    {
                        toastr.error("Não foi possível enviar e-mail. Por favor, aguarde e tente mais tarde.")
                    }

                }
            })
        }
        else
        {
            if ($("#contato_nome").val() == "")
            {
                toastr.error("Você deve preencher o nome");
                $("#contato_nome").focus();
            }
            else  if($("#contato_email").val() == "")
            {
                toastr.error("Você deve preencher o e-mail");
                $("#contato_email").focus();
            }
            else if ($("#contato_mensagem").val() == "")
            {
                toastr.error("Você deve preencher a mensagem");
                $("#contato_mensagem").focus();
            }


        }




    });
})

var view = rivets.bind($("#template-site"), c);

c.init();


function issetor(it, padrao)
{
    if(typeof it == typeof undefined || it == '')
        return padrao;

    if(it == "true")
        return true;
    else if (it == "false")
        return false;


    return it;
}

function setLoading (el, c) {

    if(c)
    {
        el.addClass("loading");
        el.append('<div class="loading-gif"></div>');

    }
    else
    {
        el.removeClass("loading")
        el.find('.loading-gif').remove();

    }
}

function trataErros (errs)
{
    var i = 0;
    $.each(errs, function(field, message)
    {
        toastr.error(message, "Atenção.");

        $("[name='"+field+"']").addClass("invalid");

        if(i == 0)
        {
            $("[name='"+field+"']").focus();
        }
        i++;
    })
}