/**
 * View Controller
 * @type {{}}
 */
var c = {};

c.acompanhar_pagamento = false;
c.pagamento_reprovado = false;
c.id_acompanhamento = 0;
c.mostra_loading = true;

c.status = "Processando";

/**
 * Função inicializadora
 */
c.init = function(){

    //Inicializa cartão
    c.card = new Card({

        form: 'form',
        container: '.card-wrapper',

        formSelectors: {
            numberInput: 'input#numero',
            expiryInput: 'input#expira_em',
            cvcInput: 'input#cvc',
            nameInput: 'input#nome'
        },

        formatting: true, // optional - default true

        messages: {
            validDate: 'valid\ndate', // optional - default 'valid\nthru'
            monthYear: 'mm/yyyy', // optional - default 'month/year'
        },

        placeholders: {
            number: '•••• •••• •••• ••••',
            name: 'Nome Completo',
            expiry: '••/••',
            cvc: '•••'
        },

        masks: {
            cardNumber: '•' // optional - mask card number
        },

        // if true, will log helpful messages for setting up Card
        debug: false // optional - default false
    });

}

/**
 * Consulta o Status de Pagamento
 */
c.consulta_status_pagamento = function(){

    c.mostra_loading = true;

    //Consulta API para verificar o status de pagamento
    $.ajax({
        url: base_api + "Pagamento/acompanhar_pagamento/" + c.id_acompanhamento,
        dataType: "JSON",
        type:"POST",

        //Caso sucesso
        success:function(resp){

            //Se obtiver sucesso
            if(resp.status)
            {
                c.status = resp.data.status;
                c.slug = resp.data.slug;
            }
        }
    })
    //Repete até que o processamento efetue
    .always(function(){

        //Se tiver processando ainda, continua as requisições
        if(c.slug == "aguardando_processamento")
        {
            $(".aviso-msg").addClass("text-primary");
            $(".aviso-msg").removeClass("text-danger");

            setTimeout(function(){

                //Efetua consulta novamente
                c.consulta_status_pagamento();

            }, 3000)
        }

        //Se for reprovado, exibe opção de voltar e efetuar pagamento novamente
        else if (c.slug == "pagamento_recusado")
        {
            $("#numero").val("");
            $("#cvc").val("");
            $("#expira_em").val("");
            $("#numero").focus();

            $(".aviso-msg").removeClass("text-success");
            $(".aviso-msg").addClass("text-danger");

            c.mostra_loading = false;
            c.pagamento_reprovado = true;
        }

        //Se for aprovado o pagamento
        else if(c.slug == "pagamento_aprovado")
        {
            //Encaminha para página de sucesso
            window.location.href = base_url + "Pagamento/sucesso/" + c.id_acompanhamento;
        }

    })

    //@todo CRON está aqui para fins de teste, porém originalmente ficaria no CRON ou CRONTAB do sistema
    setTimeout(function(){
        $.ajax({
            url: base_api + "Pagamento/Cron",
            success: function (resp) {

            }
        })
    }, 3000)

}


/**
 * Efetua um novo pagamento
 */
c.novo_pagamento = function()
{
    //Seta flags para voltar ao pagamento via cartão
    c.pagamento_reprovado = false;
    c.acompanhar_pagamento = false;
}

/**
 * Submete formulário
 */
c.submeter_formulario = function (el, dt) {

    $(el.currentTarget).attr("disabled","disabled")
    $(el.currentTarget).html("AGUARDE");

    //Submete formulário do CC para pagamento
    submeter_formulario($("form"), undefined, {

        //Habilitar função de Callback
        callback: true,

        //Quando Sucesso
        sucesso: function(resp){

            //Status de Pagamento
            if(resp.status)
            {
                //ID de acompanhamento
                c.id_acompanhamento = resp.data;
                c.acompanhar_pagamento = true;

                //Consulta o status de pagamento
                c.consulta_status_pagamento();
            }

        },
        always:function(resp){

            $(el.currentTarget).removeAttr("disabled")
            $(el.currentTarget).html("CONFIRMAR PAGAMENTO");

        }
    })

}

/**
 * Carrega binding da view
 */
var view = rivets.bind($("#template"), c);
c.init();