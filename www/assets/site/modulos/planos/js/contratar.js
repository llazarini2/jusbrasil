/**
 * View Controller
 * @type {{}}
 */
var c = {};

c.usuario = {};
c.telefones = []

c.estados = []
c.cidades = []

/**
 * Função inicializadora
 */
c.init = function(){

    //Adicionar o primeiro telefone ao cadastro
    c.adicionar_telefone();

    //Carrega os estados
    c.carrega_estados();

    //Seta plano inicial selecionado
    c.seta_plano_selecionado();

}



/**
 * Carrega os estados
 */
c.carrega_estados = function(){

    var params = {
        filter: {
            filters:[
                { field: 'tipo', value: 'estado' }
            ]
        }
    }

    //Adiciona disabled para busca
    $("#estado").attr("disabled","disabled");

    $.ajax({
        url: base_api + "Requisicao/get_all/localizacao?" + $.param(params),
        dataType: 'json',
        type: 'POST',
        success: function (resp) {

            if(resp.status)
            {
                c.estados = resp.data;
            }
            else
            {
                toastr.error("Não foi possível buscar os estados.")
            }
        }
    }).always(function()
    {
        //retira disabled do estado
        $("#estado").removeAttr("disabled");
    })

}

/**
 * Busca o CEP
 */
c.buscar_cep = function(el, dt){



    if(c.cep.length == 9)
    {
        $(el.currentTarget).attr("disabled","disabled")


        $.ajax({
            url: "https://viacep.com.br/ws/"+c.cep+"/json/",
            dataType: "JSON",
            success:function(resp){

                if(resp.erro != true)
                {
                    c.bairro = resp.bairro;
                    c.logradouro = resp.logradouro;

                    //Busca estado e seta ele
                    var estado = array_search(c.estados, "descricao", resp.uf);
                    c.estado = estado.localizacao_id;

                    //Mudou estado e seta cidade
                    c.mudou_estado(el, dt, resp.localidade)

                    //Foca no número
                    $("#numero").focus();
                }
                else
                {
                    toastr.error("Este CEP não foi encontrado, verifique se está correto.")
                }

            }
        }).always(function()
        {
            $(el.currentTarget).removeAttr("disabled")
        })

    }
    else
    {
        //Mostra mensagem de CEP inválido
        toastr.error("O CEP é inválido")

    }



}

/**
 * Mudou o estado, busca as cidades
 */
c.mudou_estado = function(el, dt, cidade){

    var estado_id = $("#estado").val();

    var params = {
        filter: {
            filters:[
                { field: 'localizacao_pai_id', value: estado_id, operator:"=" }
            ]
        },
        sort: [
            { field: "descricao", dir:"asc" }
        ]
    }

    $("#cidade").attr("disabled","disabled");

    //Busca cidade
    $.ajax({
        url: base_api + "Requisicao/get_all/localizacao?" + $.param(params),
        dataType: 'json',
        type: 'POST',
        success: function (resp) {

            if(resp.status)
            {
                c.cidades = resp.data;

                console.log("CID!!", cidade)

                //Se possuir cidade e resposta for OK
                if(typeof cidade != typeof undefined){

                    //Seta a cidade
                    var cidade_obj = array_search(c.cidades, "descricao", cidade)

                    console.log(cidade)
                    c.cidade = cidade_obj.localizacao_id;

                }
            }
            else
            {
                toastr.error("Não foi possível buscar os estados.")
            }
        }
    }).always(function(){

        $("#cidade").removeAttr("disabled");

    })

}

/**
 * Adiciona um telefone
 */
c.adicionar_telefone = function(ev, dt){

    //Adiciona telefones
    c.telefones.push({

    })

    //Atualiza máscaras
    set_masks();

}

/**
 * Remove um telefone
 * @param ev
 * @param dt
 */
c.remover_telefone = function(ev, dt){

    if(c.telefones.length - 1 >= 1)
    {
        c.telefones.splice(dt.index, 1);
    }
    else
    {
        toastr.error("É necessário ao menos um telefone cadastrado.")
    }

}

/**
 * Submete formulário para validação e (se válido) para cadastro
 */
c.submeter_formulario = function(el, dt){

    //Seta botão desabilitado
    $(el.currentTarget).attr("disabled","disabled")
    $(el.currentTarget).html("AGUARDE");

    //Submete formulário através da biblioteca auxiliar
    submeter_formulario($("form"), el, {
        callback: true,
        always: function(resp)
        {
            //Remove desabilitado
            $(el.currentTarget).removeAttr("disabled")
            $(el.currentTarget).html("CONTINUAR PARA O PAGAMENTO");
        },
    });

}

/**
 * Escolhe um plano
 * @param el
 * @param dt
 */
c.escolher_plano = function (el,dt) {

    //Seta o Plano_id
    $("#plano_id").val($(el.currentTarget).attr("plano_id"));

    c.seta_plano_selecionado();

}


/**
 * Seta plano selecionado
 */
c.seta_plano_selecionado = function(){

    $(".planos .plano").removeClass("selecionado");
    $(".planos .plano[plano_id='" + $("#plano_id").val() + "']").addClass("selecionado");

}

/**
 * Carrega binding da view
 */
var view = rivets.bind($("#template"), c);
c.init();


