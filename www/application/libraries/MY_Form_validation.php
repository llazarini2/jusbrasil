<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{

    /**
     * Valida se o endereço de e-mail ja esta sendo usado
     * @param $email
     * @return bool
     */
    public function check_email_usuario_exists($email)
    {

        $this->set_message('check_email_usuario_exists', 'Uma conta com este %s já está cadastrada.');
        $this->CI->load->model('loja_usuario_model');
        return !$this->CI->loja_usuario_model->get_by(array(
            'email' => $email,
        ));
    }

    /**
     * Valida o telefone
     * @param $telefone
     * @return bool
     */
    public function required($str)
    {
        $this->set_message('required', 'Você deve preencher o campo %s.');

        return parent::required($str);
    }

    /**
     * Valida mês / ano para cartão de crédito
     * @param $str
     * @return bool
     */
    public function valida_mes_ano($str)
    {
        $this->set_message('valida_mes_ano', 'O campo %s deve estar no formato mês/ano.');

        //Explode para retornar valor em formato de data
        $expira = explode("/", $str);

        //Verifica se expira tem duas posições
        if(sizeof($expira) == 2)
        {
            //Efetua trim
            $expira[0] = trim($expira[0]);
            $expira[1] = trim($expira[1]);



            //Valida se é um mês e ano validos
            if(strlen($expira[0]) == 2 && (int) $expira[0] <= 12 && strlen($expira[1]) == 2
                && (int) ('20' . ($expira[1])) >= (int)date("Y"))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Valida o telefone
     * @param $telefone
     * @return bool
     */
    public function validate_telefone($telefone)
    {
        $this->set_message('validate_telefone', 'O %s não é válido.');

        $telefone = app_retorna_numeros($telefone);

        if (strlen($telefone) >= 10 && strlen($telefone) <= 11) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Verifica se um campo é unico
     * @param $val
     * @param $values
     * @return bool
     */
    public function unico($val, $values)
    {
        $this->set_message('unico', 'O campo %s deve ser único.');

        $values = explode(",", $values);
        if(sizeof($values) == 3)
        {
            $table = trim($values[0]);
            $id = trim($values[1]);
            $field = trim($values[2]);
        }
        else
        {
            exit("Validação UNICO mal formatada.");
        }


        $this->CI->load->model("{$table}_model", "current_model");

        $count = $this->CI->current_model
            ->where("$field", "=", $val)
            ->getTotal();

        if($id_val = $this->CI->input->post($id))
        {
            $count_this = $this->CI->current_model
                ->where("$id", "=", $id_val)
                ->where("$field", "=", $val)
                ->getTotal();

            if($count > 1 || $count_this == 0 && $count > 0)
                return false;
        }
        else
        {
            if($count > 0)
                return false;
        }
        return true;

    }

    public function check_email_usuario_owner($email)
    {

        $this->set_message('check_email_usuario_owner', 'O %s ja esta cadastrado.');
        $this->CI->load->model('usuario_model');
        //verifica se o email existe
        if($this->CI->usuario_model->check_email_exists($email)){

            //verifica se é o dono do e-mail
            if($this->CI->usuario_model->check_email_owner($email, $this->CI->input->post('usuario_id') )){

                return true;

            } else {

                return false;
            }
        }else {

            return true;
        }

    }

    /**
     * Valida se o endereço de e-mail ja esta sendo usado
     * @param $email
     * @return bool
     */
    public function check_email_agente_exists($email)
    {

        $this->set_message('check_email_agente_exists', 'O %s ja esta cadastrado.');
        $this->CI->load->model('agente_model');
        return !$this->CI->agente_model->check_email_exists($email);
    }

    public function check_email_agente_owner($email)
    {

        $this->set_message('check_email_agente_owner', 'O %s ja esta cadastrado.');
        $this->CI->load->model('agente_model');
        //verifica se o email existe
        if($this->CI->agente_model->check_email_exists($email)){

            //verifica se é o dono do e-mail
            if($this->CI->agente_model->check_email_owner($email, $this->CI->input->post('agente_id') )){

                return true;

            }else {

                return false;
            }
        }else {

            return true;
        }

    }

    /**
     * Valida uma placa
     * @param $str
     * @return bool
     */
    function valida_placa($str)
    {
        $this->set_message('valida_placa', 'A %s não é válida.');

        if(preg_match("/[a-zA-Z]{3}-[0-9]{4}/", $str))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }

    }


    /**
     * Valida se é o menor campo
     * @param string $str
     * @param $parametros
     * @return bool
     */
    function menor_campo($str ='', $parametros)
    {
        $parametros = explode(',', $parametros);
        $campo = str_replace('.',',', $this->retorna_numeros_valor($_POST[$parametros[0]]));
        $valor = $campo*(1-($parametros[1]/100));

        $this->set_message('menor_campo', 'O %s deve ser menor ou igual a '.$valor);
        if(isset($campo))
            if($str <= $valor)
            {
                return true;
            }
        return false;
    }

    /**
     * Retorna números
     * @param $str
     * @return string|string[]|null
     */
    function retorna_numeros($str)
    {
        return preg_replace('/[^0-9]/', '', $str);
    }

    /**
     * Retorna números do valor
     * @param $str
     * @return string|string[]|null
     */
    function retorna_numeros_valor($str)
    {
        return  preg_replace('/[^0-9,]/', '', str_replace('.','',$str));
    }

    /**
     * Retira Underline
     * @param $str
     * @return mixed
     */
    function retira_underline($str)
    {
        return str_replace('_', '', $str);
    }

    /**
     * Valida se possui espaços ou não
     * @param $string
     * @return bool
     */
    function valida_slug($str)
    {
        $this->set_message('valida_slug', 'O %s não pode possuir espaços ou caracteres especiais.');
        if(preg_match('/^[a-z][-a-z0-9_]*$/', $str)){
            return true;
        } else {
            return false;
        }

    }

    /**
     * Valida se não possui espaços
     * @param $str
     * @return bool
     */
    function valida_sem_espaco($str)
    {
        $value = strrpos($str, ' ');
        $this->set_message('valida_sem_espaco', 'O %s nao pode possuir espacos.');
        

        if($value == false)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Valida CPF
     * @param $cpf
     * @return bool
     */
    public function validate_cpf($cpf){

        $this->set_message('validate_cpf', 'O número de CPF informado no campo %s é inválido.');
        return app_validate_cpf($cpf);

    }

    /**
     * Valida CNPJ
     * @param $cnpj
     * @return mixed
     */
    public function validate_cnpj($cnpj){
        $this->set_message('validate_cnpj', 'O número do CNPJ informado no campo %s é inválido.');
        return app_validar_cnpj($cnpj);
    }

    /**
     * Valida CEP
     * @param $cep
     * @return bool
     */
    public function validate_cep($cep){
        $this->set_message('validate_cep', 'O %s informado é inválido.');


        if(strlen($this->retorna_numeros($cep)) == 8)
        {
            return true;
        }

        return false;
    }

}