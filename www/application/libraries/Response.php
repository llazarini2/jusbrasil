<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Response
 * Classe auxiliar para API simples
 */
class Response
{
    private $message = "Não foi possível efetuar a consulta.";
    private $dados = array();
    private $status = false;
    private $total = 0;
    private $error;
    private $redirect = "";
    private $ci;

    function __construct()
    {
        $this->ci = &get_instance();
    }

    public function setRedirect($red)
    {
        $this->redirect = $red;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @param array $data
     */
    public function setDados($data)
    {
        $this->dados = $data;
    }

    /**
     * @param array $data
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return array
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param array $data
     */
    public function setError($data)
    {
        $this->error = $data;
    }

    /**
     * Codifica para UTF-8
     * @return array
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Codifica para UTF-8
     * @return array
     */
    public function getDados()
    {
        if(is_array($this->dados))
            return utf8_converter($this->dados);
        return $this->dados;
    }

    /**
     * @return boolean
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param boolean $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * Retorna JSON
     * @return string
     */
    public function getJSON()
    {
        $data = array();
        $data['status'] = $this->isStatus();
        $data['message'] = $this->getMessage();
        $data['data'] = $this->getDados();
        $data['total'] = $this->getTotal();
        $data['redirect'] = $this->redirect;

        //Verifica se necessário dar clean no buffer
        if(trim(ob_get_contents()) != "")
            ob_end_clean();

        $this->ci->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

}

