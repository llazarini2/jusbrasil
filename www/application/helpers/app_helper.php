<?php

/**
 * Retorna assets URL
 * @param string $uri
 * @param string $context
 * @return string
 */
function app_assets_url($uri = '', $context = 'site'){

    $CI =& get_instance();
    $assets_directory = $CI->config->item('app_assets_dir');

    $uri = $assets_directory . '/' .$context . '/' . app_get_version_url($uri);

    return base_url($uri);
}

/**
 * Retorna URL com versionamento dos assets automático
 * @param $uri
 * @return string
 */
function app_get_version_url($uri)
{
    $CI =& get_instance();
    $assets_version = $CI->config->item('app_assets_ver');

    return $uri;
}

/**
 * Retorna diretório assets
 * @param string $uri
 * @param string $context
 * @return string
 */
function app_assets_dir($uri = '', $context = 'site')
{
    $CI =& get_instance();
    $assets_directory = $CI->config->item('app_assets_dir');
    $uri = $assets_directory . '/' .$context . '/' .$uri;
    return dirname(__FILE__) . "/../../{$uri}/";
}

/**
 * Retorna controller atual
 * @return mixed
 */
function app_current_controller()
{
    $CI =& get_instance();
    return $CI->router->fetch_class();
}

/**
 * Retorna método atual
 * @return mixed
 */
function app_current_method()
{
    $CI =& get_instance();
    return $CI->router->fetch_method();
}

/**
 * Verifica se é o controller atual
 * @param $controller
 * @return bool
 */
function is_current_controller($controller){

    if(is_array($controller)){
       if(in_array(app_current_controller(), $controller )){
           return  true;
       }else {
           return  false;
       }
    }else{
        if($controller == app_current_controller()){
            return  true;
        }else {
            return  false;
        }
    }
}

/**
 * Verifica se é método atual
 * @param $method
 * @return bool
 */
function is_current_method($method){

    if($method == app_current_method()){

        return  true;
    }else {
        return  false;
    }
}

function app_get_current_page()
{
    $url = current_url();

    if(strpos("#", $url) > 0)
    {
        $url = explode("#", $url);

        if(is_array($url) && sizeof($url) == 2)
        {
            return $url[1];
        }
    }
    return $url;


}

function validation_errors_array($prefix = '', $suffix = '') {
    if (FALSE === ($OBJ = & _get_validation_object())) {
        return '';
    }

    return $OBJ->error_array($prefix, $suffix);
}

function app_date_mysql_to_mask($date, $format = 'd/m/Y H:i'){
   if($date != '0000-00-00 00:00:00' && $date != '') {
    return date($format, strtotime($date));
   }else {
    return '';
    }
}


function app_date_mask_to_mysql($date, $again = false)
{
    $orig = $date;
    if($date != '0000-00-00 00:00:00' && $date != '')
    {
        $date = preg_split('/\s/', trim($date));

        if(count($date) == 2){

            $dia = $date[0];
            $hora = $date[1];

            $date = implode('/', array_reverse(explode('/', $dia))) . ' ' . $hora;
            return date("Y-m-d H:i:s", strtotime($date));
        }
    }

    if(!$again)
    {
        return app_date_mask_to_mysql($orig . " 00:00:00", true);
    }
    return '';
}

function app_get_mes_numerico($mes)
{
    $meses = array(
        'JAN' => '01',
        'FEV' => '02',
        'FEB' => '02',
        'MAR' => '03',
        'ABR' => '04',
        'APR' => '04',
        'MAI' => '05',
        'MAY' => '05',
        'JUN' => '06',
        'JUL' => '07',
        'AGO' => '08',
        'AUG' => '08',
        'SET' => '09',
        'SEP' => '09',
        'OUT' => '10',
        'OCT' => '10',
        'NOV' => '11',
        'DEZ' => '12',
        'DEC' => '12',
    );

    return issetor($meses[$mes], false);
}

function app_date_get_diff_dias($d1, $d2, $type=''){

    if(!empty($d1) && !empty($d2)) {

        $d1 = explode('/', $d1);
        $d2 = explode('/', $d2);
        $type = strtoupper($type);
        switch ($type)
        {
            case 'Y':
                $X = 31536000;
                break;
            case 'M':
                $X = 2592000;
                break;
            case 'D':
                $X = 86400;
                break;
            case 'H':
                $X = 3600;
                break;
            case 'I':
                $X = 60;
                break;
            default:
                $X = 1;
        }
        return floor( (mktime(0, 0, 0, $d2[1], $d2[0], $d2[2]) - mktime(0, 0, 0, $d1[1], $d1[0], $d1[2] ) )/$X );

    }else{
        return 0;
    }


}

function app_dateonly_mask_to_mysql($date)
{
    if ($date != '0000-00-00' && $date != '') {
        return $date = implode('-', array_reverse( explode('/', $date) ) );
    } else {
        return '';
    }
}

function app_dateonly_mysql_to_mask($date)
{
    if($date != '0000-00-00' && $date != '') {
        return date("d/m/Y", strtotime($date));
    }else {
        return '';
    }
}
function app_cpf_to_mask($cpf)
{
    $string = substr($cpf, 0, 3).'.'.substr($cpf, 3,3).'.'.substr($cpf, 6,3).'-'.substr($cpf, 9, 2);
    return $string;
}

function app_cnpj_to_mask($cpf)
{
    $string = substr($cpf, 0, 2).'.'.substr($cpf, 2,3).'.'.substr($cpf, 5,3).'/'.substr($cpf, 8, 4).'-'.substr($cpf, 12,2);
    return $string;
}

function app_verifica_cpf_cnpj ($cpf_cnpj) {
    // Verifica CPF
    if ( strlen($cpf_cnpj ) === 11 ) {
        return 'CPF';
    }
    // Verifica CNPJ
    elseif ( strlen( $cpf_cnpj ) === 14 ) {
        return 'CNPJ';
    }
    // NÃ£o retorna nada
    else {
        return false;
    }
}

function app_clear_number($str){

    return preg_replace('/[^0-9]/', '', $str);
}

function app_char_alpha($index){
    //fixa o problema do indice do array comeÃ§ar em zero;
    $index = $index - 1;

    $char_list = range('A', 'Z' );

    return $char_list[$index];
}
function app_get_querystring_full(){
    $query = '';
    $url = parse_url($_SERVER['REQUEST_URI']);

    if(isset($url['query'])){

        $query = '?' . $url['query'];
    }

    return$query;
}

function app_get_value($field, $default = ''){
    $ci = & get_instance();


    if(is_array($field)){

        foreach ($field as $key => $value){

            if(isset($_GET[$key][$value])){

                return $_GET[$key][$value];
            }else {

                return $default;
            }

        }
    }
    if(isset($_GET[$field])){

        return $_GET[$field];
    }else {

        return $default;
    }


}

function app_validate_cpf($cpf) {

    // Elimina possivel mascara
    $cpf = preg_replace('/[^0-9]/', '', $cpf);
    $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

    // Verifica se o numero de digitos informados Ã© igual a 11
    if (strlen($cpf) != 11) {
        return false;
    }
    // Verifica se nenhuma das sequÃªncias invalidas abaixo
    // foi digitada. Caso afirmativo, retorna falso
    else if ($cpf == '00000000000' ||
        $cpf == '11111111111' ||
        $cpf == '22222222222' ||
        $cpf == '33333333333' ||
        $cpf == '44444444444' ||
        $cpf == '55555555555' ||
        $cpf == '66666666666' ||
        $cpf == '77777777777' ||
        $cpf == '88888888888' ||
        $cpf == '99999999999') {

        return false;
        // Calcula os digitos verificadores para verificar se o
        // CPF Ã© vÃ¡lido
    } else {

        for ($t = 9; $t < 11; $t++) {

            for ($d = 0, $c = 0; $c < $t; $c++) {
                $d += $cpf{$c} * (($t + 1) - $c);
            }
            $d = ((10 * $d) % 11) % 10;
            if ($cpf{$c} != $d) {

                return false;
            }
        }

        return true;
    }
}

function app_validate_cnpj($cnpj)
{
    $cnpj = trim($cnpj);
    $soma = 0;
    $multiplicador = 0;
    $multiplo = 0;


    # [^0-9]: RETIRA TUDO QUE NÃƒO Ã‰ NUMÃ‰RICO,  "^" ISTO NEGA A SUBSTITUIÃ‡ÃƒO, OU SEJA, SUBSTITUA TUDO QUE FOR DIFERENTE DE 0-9 POR "";
    $cnpj = preg_replace('/[^0-9]/', '', $cnpj);

    if(empty($cnpj) || strlen($cnpj) != 14)
        return FALSE;

    # VERIFICAÃ‡ÃƒO DE VALORES REPETIDOS NO CNPJ DE 0 A 9 (EX. '00000000000000')
    for($i = 0; $i <= 9; $i++)
    {
        $repetidos = str_pad('', 14, $i);

        if($cnpj === $repetidos)
            return FALSE;
    }

    # PEGA A PRIMEIRA PARTE DO CNPJ, SEM OS DÃGITOS VERIFICADORES
    $parte1 = substr($cnpj, 0, 12);

    # INVERTE A 1Âª PARTE DO CNPJ PARA CONTINUAR A VALIDAÃ‡ÃƒO    $parte1_invertida = strrev($parte1);
    $parte1_invertida = strrev($parte1);
    # PERCORRENDO A PARTE INVERTIDA PARA OBTER O FATOR DE CALCULO DO 1Âº DÃGITO VERIFICADOR
    for ($i = 0; $i <= 11; $i++)
    {
        $multiplicador = ($i == 0) || ($i == 8) ? 2 : $multiplicador;

        $multiplo = ($parte1_invertida[$i] * $multiplicador);

        $soma += $multiplo;

        $multiplicador++;
    }

    # OBTENDO O 1Âº DÃGITO VERIFICADOR
    $rest = $soma % 11;

    $dv1 = ($rest == 0 || $rest == 1) ? 0 : 11 - $rest;

    # PEGA A PRIMEIRA PARTE DO CNPJ CONCATENANDO COM O 1Âº DÃGITO OBTIDO
    $parte1 .= $dv1;

    # MAIS UMA VEZ INVERTE A 1Âª PARTE DO CNPJ PARA CONTINUAR A VALIDAÃ‡ÃƒO
    $parte1_invertida = strrev($parte1);

    $soma = 0;

    # MAIS UMA VEZ PERCORRE A PARTE INVERTIDA PARA OBTER O FATOR DE CALCULO DO 2Âº DÃGITO VERIFICADOR
    for ($i = 0; $i <= 12; $i++)
    {
        $multiplicador = ($i == 0) || ($i == 8) ? 2 : $multiplicador;

        $multiplo = ($parte1_invertida[$i] * $multiplicador);

        $soma += $multiplo;

        $multiplicador++;
    }

    # OBTENDO O 2Âº DÃGITO VERIFICADOR
    $rest = $soma % 11;

    $dv2 = ($rest == 0 || $rest == 1) ? 0 : 11 - $rest;

    # AO FINAL COMPARA SE OS DÃGITOS OBTIDOS SÃƒO IGUAIS AOS INFORMADOS (OU A SEGUNDA PARTE DO CNPJ)
    return ($dv1 == $cnpj[12] && $dv2 == $cnpj[13]) ? TRUE : FALSE;
}

function app_db_escape($string){

    $ci = & get_instance();

    return $ci->db->escape($string);
}

function app_set_value($field = '', $default = '')
{



        if ( ! isset($_POST[$field]))
        {
            $value =  $default;
        }else {

            $value =  $_POST[$field];
        }



    return form_prep($value, $field);
}

function app_has_value($field = '')
{



    if (isset($_POST[$field]))
    {
        return true;
    }else {

        return false;
    }
}

/**
 * @description  Formata telefone (11)X99999999
 * @param $numero
 * @return mixed|string
 */
function app_format_telefone($numero){

    $data = app_extract_telefone($numero);

    return (empty($data['ddd'])) ? $data['numero'] : "({$data['ddd']}){$data['numero']}";
}


function app_extract_telefone($numero){
    $numero = preg_replace('/([^0-9])/','',$numero);

    $data = array(
        'numero' => $numero,
        'ddd' => '',
    );

    if(strlen($numero) == 8 || strlen($numero) == 9 ){

        $data['numero'] = $numero;
        $data['ddd'] = '';

    }
    if(strlen($numero) == 10  || strlen($numero) == 11 ){

        $data['numero'] = substr($numero, 2);
        $data['ddd'] =  substr($numero, 0, 2);

    }

    return $data;
}


if ( ! function_exists('app_get_random_password'))
{

    function app_get_random_password($chars_min=6, $chars_max=8, $use_upper_case=false, $include_numbers=false, $include_special_chars=false)
    {
        $length = rand($chars_min, $chars_max);
        $selection = 'aeuoyibcdfghjklmnpqrstvwxz';
        if($include_numbers) {
            $selection .= "1234567890";
        }
        if($include_special_chars) {
            $selection .= "!@\"#$%&[]{}?|";
        }

        $password = "";
        for($i=0; $i<$length; $i++) {
            $current_letter = $use_upper_case ? (rand(0,1) ? strtoupper($selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))]) : $selection[(rand() % strlen($selection))];
            $password .=  $current_letter;
        }

        return $password;
    }

}

function app_get_select_html($field_select, $field_label,  $valores, $valor_selecionado, $valor_opt = 'chave', $texto_in='', $atributos = ''){
    $html = "";
    $html .= "\n<select name=\"{$field_select}\" id=\"{$field_select}\" {$atributos}>\n";
    if (!is_array($valor_selecionado) && !empty($texto_in)) $html .= "\t<option value=\"\">$texto_in</option>\n";
    foreach ($valores as  $valor){
        if($valor_opt == 'chave'){
            $html .= "\t<option value=\"{$valor[$field_select]}\"";
            if(is_array($valor_selecionado)){
                if(in_array($valor[$field_select], $valor_selecionado)){
                    $html .= " selected";
                }
            }else{
                if("{$valor[$field_select]}" == "{$valor_selecionado}") $html .= " selected";
            }
        } else {
            $html .= "\t<option value=\"{$valor[$field_label]}\"";
            if($valor[$field_label] == $valor_selecionado) $html .= " selected";
        }
        $html .= ">{$valor[$field_label]}</option>\n";
    }
    $html .= "</select>\n";
    return $html;
}



function app_get_km_list(){

    return array(
         0 =>'0 Km',
         1000 => '1000 Km',
         5000 => '5000 Km',
         10000 => '10000 Km',
         20000 => '20000 Km',
         30000 => '30000 Km',
         40000 => '40000 Km',
         50000 => '50000 Km',
         60000 => '60000 Km',
         70000 => '70000 Km',
         80000 => '80000 Km',
         90000 => '90000 Km',
         100000 => '100000 Km',
         110000 => '110000 Km',
         120000 => '120000 Km',
         130000 => '130000 Km',
         140000 => '140000 Km',
         150000 => '150000 Km'
    );
}

function app_youtube_id_by_url($url){

    $url = parse_url($url);

    if(isset($url['query'])){
        parse_str($url['query'], $data);
        if(isset($data['v'])){
            return $data['v'];
        }
    }
    return false;

}

function app_youtube_image_by_id($id, $scope = 'default'){

   $img = "http://img.youtube.com/vi/{$id}/{$scope}.jpg";
   return $img;

}
function app_youtube_embed_link_by_id($id){

    $img = "http://www.youtube.com/embed/{$id}?autoplay=1";
    return $img;

}

function app_get_slug($string) {

    $table = array(
        'Å '=>'S', 'Å¡'=>'s', 'Ä'=>'Dj', 'Ä‘'=>'dj', 'Å½'=>'Z', 'Å¾'=>'z', 'ÄŒ'=>'C', 'Ä'=>'c', 'Ä†'=>'C', 'Ä‡'=>'c',
        'Ã€'=>'A', 'Ã'=>'A', 'Ã‚'=>'A', 'Ãƒ'=>'A', 'Ã„'=>'A', 'Ã…'=>'A', 'Ã†'=>'A', 'Ã‡'=>'C', 'Ãˆ'=>'E', 'Ã‰'=>'E',
        'ÃŠ'=>'E', 'Ã‹'=>'E', 'ÃŒ'=>'I', 'Ã'=>'I', 'ÃŽ'=>'I', 'Ã'=>'I', 'Ã‘'=>'N', 'Ã’'=>'O', 'Ã“'=>'O', 'Ã”'=>'O',
        'Ã•'=>'O', 'Ã–'=>'O', 'Ã˜'=>'O', 'Ã™'=>'U', 'Ãš'=>'U', 'Ã›'=>'U', 'Ãœ'=>'U', 'Ã'=>'Y', 'Ãž'=>'B', 'ÃŸ'=>'Ss',
        'Ã '=>'a', 'Ã¡'=>'a', 'Ã¢'=>'a', 'Ã£'=>'a', 'Ã¤'=>'a', 'Ã¥'=>'a', 'Ã¦'=>'a', 'Ã§'=>'c', 'Ã¨'=>'e', 'Ã©'=>'e',
        'Ãª'=>'e', 'Ã«'=>'e', 'Ã¬'=>'i', 'Ã­'=>'i', 'Ã®'=>'i', 'Ã¯'=>'i', 'Ã°'=>'o', 'Ã±'=>'n', 'Ã²'=>'o', 'Ã³'=>'o',
        'Ã´'=>'o', 'Ãµ'=>'o', 'Ã¶'=>'o', 'Ã¸'=>'o', 'Ã¹'=>'u', 'Ãº'=>'u', 'Ã»'=>'u',  'Ã½'=>'y', 'Ã¾'=>'b',
        'Ã¿'=>'y', 'Å”'=>'R', 'Å•'=>'r', '/' => '-', ' ' => '-'
    );

    return strtolower(strtr($string, $table));
}

function app_get_url_anuncio($anuncio){

    $CI =& get_instance();
    $CI->load->model('anuncio_model');
    return $CI->anuncio_model->getAnuncioUrl($anuncio);

}
function app_get_url_acessorio($anuncio){

    $CI =& get_instance();
    $CI->load->model('acessorio_anuncio_model', 'acessorio_anuncio');
    return $CI->acessorio_anuncio->getAnuncioUrl($anuncio);

}



function app_merge_query_string($data){

    $data = (array) $data;
    $url_parts = parse_url($_SERVER['REQUEST_URI']);

    if(isset($url_parts['query'])){



        parse_str( $url_parts['query'], $query);


        $query = array_merge($query, $data );



        $url = http_build_query($query);

    }else {

        $url = http_build_query($data);

    }

    return $url;
}

function app_html_escape_br($string){
    return nl2br(html_escape($string));
}


function app_get_userdata($item){
    $CI =& get_instance();
    return $CI->session->userdata($item);

}
function app_format_currency($number, $symbol = false, $num_casas = 2){

    return number_format($number, $num_casas, ',' , '.');
}

function app_get_toprides($limit = 4){
    $CI = &get_instance();

    $CI->load->model('anuncio_model', 'anuncio_model');
    return $CI->anuncio_model->getSidebarTopRides($limit);

}

function app_get_banner_by_codigo($codigo, $rand = true){

    $CI = &get_instance();

    $CI->load->model('cms_banner_model', 'cms_banner');

    return $CI->cms_banner->getBannerByCodigo($codigo, $rand );

}

function app_get_firt_word($string){

    $words = preg_split('/\s/', $string);

    $word = trim($words[0]);
    return ucfirst($word);
}
function app_unformat_currency($value){

    $clearValue = $value;

    return (float) str_replace(',', '.',  str_replace('.', '',$clearValue));
}

function app_unformat_percent($value){

    $clearValue = preg_replace('/([^0-9\.,])/i', '', $value);

    return str_replace(',', '.', $clearValue);
}

function app_word_cut($string, $limit, $append =  '...'){

    if(strlen($string) > $limit){

        return mb_substr($string, 0, $limit, 'UTF-8' ) . $append;
    }else {

        return $string;
    }
}

function app_utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
            $item = utf8_encode($item);
        }
    });

    return $array;
}
function app_retorna_numeros($string)
{
    return preg_replace('/[^0-9]/', '', $string);
}



/**
 * retorna tipo de cartÃ£o por nÃºmero
 * @param $number
 * @return string
 */
function app_get_card_type($number)
{
    $number=preg_replace('/[^\d]/','',$number);
    if (preg_match('/^3[47][0-9]{13}$/',$number))
    {
        return 'AMEX';
    }
    elseif (preg_match('/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/',$number))
    {
        return 'DINERS';
    }
    elseif (preg_match('/^6(?:011|5[0-9][0-9])[0-9]{12}$/',$number))
    {
        return 'Discover';
    }
    elseif (preg_match('/^(?:2131|1800|35\d{3})\d{11}$/',$number))
    {
        return 'JCB';
    }
    elseif (preg_match('/^5[1-5][0-9]{14}$/',$number))
    {
        return 'MASTERCARD';
    }
    elseif (preg_match('/^4[0-9]{12}(?:[0-9]{3})?$/',$number))
    {
        return 'VISA';
    }
    else
    {
        return 'NOTACCEPTED';
    }
}

function app_dateonly_mask_mysql_null($date)
{
    if ($date != '0000-00-00' && $date != '')
    {
        return $date = implode('-', array_reverse( explode('/', $date) ) );
    }
    else
    {
        return '0000-00-00 00:00:00';
    }
}

/**
 * Monta o menu
 * @param $itens
 * @return string
 */
function app_menu($itens)
{
    $html = [];

    app_montar_menu($itens, $html);

    $html_return = "";

    if($html)
        foreach($html as $line)
            $html_return .= $line;

    return $html_return;
}

/**
 * Monta menu com itens já formatados
 * @param $itens
 * @param $html
 */
function app_montar_menu($itens, &$html)
{
    foreach ($itens as $item)
    {
        if($item['exibir_menu'])
        {
            $active = (is_current_controller($item['controllers'])) ? ' expanded ' : '';
            if(is_current_controller($item['controllers']) && count($item['itens']) == 0){
                $active = ' expanded active';
            }
            if($item['externo'] == 0)
            {
                if(empty($item['controller'])){
                    $url = "";
                }else{
                    $url = base_url("admin/{$item['controller']}/{$item['acao']}");
                    $url .= (!empty($item['acao']) && empty($item['parametros'])) ? "/{$item['parametros']}" : '';
                }
            }else{
                $url = $item['url'];
            }

            if($item['itens'])
                $url = "";

            $class_sub = (count($item['itens']) > 0) ? 'gui-folder' : '';
            $icon = (empty($item['icon']) && $item['pai_id'] == 0) ? 'fa fa-folder-open fa-fw' : $item['icon'];
            $html[] = "<li class=\"{$class_sub}{$active}\">";
            $html[] = (empty($url)) ? "    <a>" : "    <a class=\"{$active}\" la-href=\"{$url}\" target=\"{$item['target']}\" >";
            $html[] = ($item['pai_id'] == 0) ? "        <div class=\"gui-icon\"><i class=\"{$icon}\"></i></div>" : "";
            $html[] = "        <span class=\"title\">{$item['nome']}</span>";
            $html[] = "    </a>";
            if($item['itens']){
                $html[] = "<ul>";
                app_montar_menu($item['itens'], $html);
                $html[] = "</ul>";
            }

            $html[] = "</li>";
        }
    }
}

/**
 * Se for setado uma variÃ¡vel ele a retorna, caso contrÃ¡rio retorna vazio
 * @param $var
 * @param bool $default
 * @return string
 */
function issetor(&$var, $default = false) {

    if(isset($var))
    {
        return $var;
    }
    else
    {
        if($default)
            return $default;
        return "";
    }
}

/**
 * admin url
 * @param string $uri
 * @return mixed
 */
function portal_url($uri = '')
{
    return base_url() . 'portal/' . $uri;
}

/**
 * admin url
 * @param string $uri
 * @return mixed
 */
function admin_url($uri = '')
{
    return base_url() . 'admin/' . $uri;
}
/**
 * admin url
 * @param string $uri
 * @return mixed
 */
function site_url($uri = '')
{
    return base_url() . 'site/' . $uri;
}

/**
 * admin url
 * @param string $uri
 * @return mixed
 */
function service_url($uri = '')
{
    return base_url() . 'services/' . $uri;
}

/**
 * Numero para letra (excel)
 * @param $n
 * @return string
 */
function app_num2alpha($n)
{
    for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
        $r = chr($n%26 + 0x41) . $r;
    return $r;
}

function app_is_form_error($field_name){

    $error = form_error($field_name);

    return !empty($error);
}

function app_get_form_error($field_name){

    $error = form_error($field_name);

    if(!empty($error)) {
        $error = strip_tags($error);
        return "<span id=\"{$field_name}-error\" class=\"help-block\">{$error}</span>";
    }else{
        return "";
    }

}

//
/**
 * Funções para integração
 */

function app_integracao_date($formato, $dados = array()){

    return date($formato);

}

function app_integracao_get_sequencial($formato, $dados = array()){

    return str_pad($dados['log']['sequencia'], $dados['item']['tamanho'], $dados['item']['valor_padrao'], STR_PAD_LEFT);

}


function app_integracao_format_decimal($formato, $dados = array()){

    $valor = explode('.', $dados['registro'][$formato]);
    return str_pad($valor[0],13, '0', STR_PAD_LEFT) . str_pad($valor[1],2, '0', STR_PAD_LEFT);

}

function app_integracao_format_porcentagem($formato, $dados = array()){

    $valor = explode('.', $dados['registro'][$formato]);
    return str_pad($valor[0],2, '0', STR_PAD_LEFT) . str_pad($valor[1],2, '0', STR_PAD_LEFT);

}

function app_integracao_get_qnt_registros($formato, $dados = array()){

    return str_pad($dados['log']['quantidade_registros'] + 2, $dados['item']['tamanho'], $dados['item']['valor_padrao'], STR_PAD_LEFT);

}

function app_integracao_get_valor_total($formato, $dados = array()){

    $valor = 0.0;
    foreach ($dados['registro'] as $registro) {
        $valor += $registro['valor_premio_total'];
    }

    $valor = ($valor == 0) ? '0.0' : $valor;
    $valor = explode('.', $valor);
    return str_pad($valor[0],13, '0', STR_PAD_LEFT) . str_pad($valor[1],2, '0', STR_PAD_LEFT);

}

function app_calculo_valor($tipo, $quantidade, $valor){

    $result = $valor;

    if($tipo == 'PORCENTAGEM'){
        $result =  $valor - (( $quantidade / 100 ) * $valor);
    }elseif($tipo == 'MONETARIO'){
        $result = $valor - $quantidade;
    }

    return round($result, 2);
}

/**
 * Converte para UTF8 recursivamente
 * @param $array
 * @return mixed
 */
function utf8_converter($array)
{
    array_walk_recursive($array, function(&$item, $key){
        if(!mb_detect_encoding($item, 'utf-8', true)){
            $item = utf8_encode($item);
        }
    });

    return $array;
}


function app_get_acao_permitida ($acoes, $acao_id)
{
    foreach($acoes as $acao)
    {
        if($acao['usuario_acl_acao_id'] == $acao_id)
            return $acao['permitido'];
    }
    return 0;
}

function app_print_recursos($recursos)
{

    foreach($recursos as $recurso) {
        echo "
        <li class=''>
            <div idAcao='1' class='checkbox checkbox-inline checkbox-styled'>
                <label>
                    <input checked='' idAcao='1' idRecurso='{$recurso['usuario_acl_recurso_id']}' class='btRecurso' type='checkbox' name='recurso_acao[{$recurso['usuario_acl_recurso_id']}][1]'>
                    <span>{$recurso['nome']}</span>
                </label>
            </div>

            ".app_print_recurso_filho($recurso)."
        </li>
        ";
    }
}

function app_print_recurso_filho($recurso)
{

    if(isset($recurso['acoes'])) {
        echo "<div style='padding-left: 20px;' class='acoes'>";

        foreach ($recurso['acoes'] as $acao)
        {
            if($acao['usuario_acl_acao_id'] != 1)
            {
                echo "
                <div idAcao='{$acao['usuario_acl_acao_id']}' class=\"checkbox checkbox-inline checkbox-styled card-body\">
                    <label>
                        <input idAcao='{$acao['usuario_acl_acao_id']}' idRecurso='{$recurso['usuario_acl_recurso_id']}' class='btRecurso' type=\"checkbox\"";
                if($acao['permitido'])
                    echo ' checked ';
                echo "name='recurso_acao[{$recurso['usuario_acl_recurso_id']}][{$acao['usuario_acl_acao_id']}]'>
                        <span>{$acao['usuario_acl_acao_nome']}</span>
                    </label>
                </div>
                ";
            }
        }

        echo "</div>";
    }

    if(isset($recurso['filhos'])) {
        echo "<ul class='filhos' id='{$recurso['usuario_acl_recurso_id']}'>";

        foreach ($recurso['filhos'] as $filho) {
            echo "<li class='titulo'>";

            foreach ($recurso['acoes'] as $acao)
            {
                if($acao['usuario_acl_acao_id'] == 1)
                {
                    echo "
                <div idAcao='{$acao['usuario_acl_acao_id']}' class=\"checkbox checkbox-inline checkbox-styled\">
                    <label>
                        <input idAcao='{$acao['usuario_acl_acao_id']}' idRecurso='{$filho['usuario_acl_recurso_id']}' class='btRecurso' type=\"checkbox\"";
                    if($acao['permitido'])
                        echo ' checked ';
                    echo "name='recurso_acao[{$filho['usuario_acl_recurso_id']}][{$acao['usuario_acl_acao_id']}]'>
                        <span></span>
                    </label>
                </div>
                ";
                }
            }

            echo"<i class='fa fa-chevron-right'></i> {$filho['nome']}</li>";

            app_print_recurso_filho($filho);
        }

        echo "</ul>";
    }
}

/**
 * Retorna recurso atual
 * @return array
 */
function app_recurso($slug = null)
{
    if(!$slug)
        $slug = app_current_controller();
    $CI =& get_instance();
    $CI->load->model("usuario_acl_recurso_model", "recurso");
    $recurso = $CI->recurso->get_by(array("controller" => $slug));
    if($recurso)
       return $recurso;
    return array();
}

/**
 * Retorna nome do recurso atual
 * @return string
 */
function app_recurso_nome()
{
    $CI =& get_instance();
    $CI->load->model("usuario_acl_recurso_model", "recurso");
    $recurso = $CI->recurso->get_by(array("controller" => app_current_controller()));

    if($recurso && isset($recurso['nome']))
        return $recurso['nome'];
    return "";
}

/**
 * Printa menu filho
 * @param $recursos
 * @return bool|void
 */
function app_print_menu_filho_expanded($recursos)
{
    foreach($recursos as $recurso)
    {
        if(!isset($recurso['slug']))
            $recurso['slug'] = $recurso['url'];

        if($recurso['slug'] == app_current_controller())
            return true;

        else if($recurso['filhos'])
            return app_print_recurso_filho($recurso['filhos']);
    }
    return false;
}

/**
 * Printa um menu
 * @param $recursos
 * @param bool $is_filho
 * @param array $opts
 */
function app_print_menu($recursos, $is_filho = false, $opts = array())
{
    $padrao = array(
        'class' => 'gui-folder',
        'link_class' => 'fa fa-folder-open fa-fw',
        'pre_url' => 'admin',
        'parent_has_div' => true
    );

    if(empty($opts))
        $opts = $padrao;

    foreach($recursos as $recurso) {

        if(!isset($recurso['controller']))
            $recurso['controller'] = $recurso['url'];

        if($recurso['visivel']) {
            echo "
            <li class='".issetor($opts['class']);

            if(app_print_menu_filho_expanded($recurso['filhos']))
                echo " expanded ";

            echo "'>

            <a ";

            if(!$recurso['filhos'])
                echo "href='" . base_url(($opts['pre_url'])."/".$recurso['controller']) . "/".issetor($recurso['acao'])."'";
            echo ">
            ";
            if (!$is_filho && (!isset($opts['parent_has_div']) || $opts['parent_has_div'] == true)) {
                echo "
                <div class='gui-icon'>
                    <i class='".issetor($opts['link_class'])."'></i>
                </div>";
            }
            echo "
                <span class='title'>{$recurso['nome']}</span>
            </a>
        ";

            if ($recurso['filhos']) {
                echo "<ul>";

                $padrao['pre_url'] = $opts['pre_url'];

                app_print_menu($recurso['filhos'], true, $padrao);
                echo "</ul>";
            }

            echo "
            </li>
        ";
        }
    }
}

/**
 * Seta select
 * @param $value
 * @param $selected
 * @return string
 */
function app_seta_selected($value, $selected)
{
    return $value==$selected ? ' selected="selected"' : '';
}

/**
 * Retorna array de uma string
 * @param $string
 * @param $name
 * @return string
 */
function app_get_array_string($string, $name)
{
    $pos = strpos($string, $name);
    $var = substr($string, $pos);
    $var_begin = strpos($var, '[') + 1;
    $var_end = strpos($var, ']');

    $var_string = substr($var, $var_begin, $var_end - $var_begin);
    return $var_string;
}

/**
 * Seta array em string
 * @param $string
 * @param $name
 * @param $array
 * @return string
 */
function set_array_string($string, $name, $array)
{
    $var = app_get_array_string($string, $name);

    $string = substr($string, 0, strpos($string, $name));
    return $string . $array[$var];

}
/**
 * @param $path
 * @return bool
 */
function app_image_exists($path, $img)
{
    $cam = FCPATH ."assets/upload/{$path}{$img}";

    if(file_exists($cam) && is_file($cam))
    {
        return true;
    }
    return false;
}

/**
 * Verifica se é imagem
 * @param $img
 * @return bool
 */
function app_is_img($img)
{
    if(strpos(".jpg", $img))
        return true;
    else if(strpos(".png", $img))
        return true;
    else if(strpos(".gif", $img))
        return true;
    else if(strpos(".bmp", $img))
        return true;
    return false;
}
/**
 * Retorna Imagem (caso não exista, cria thumb)
 * @param $img
 * @param $path
 * @param $width
 * @param $height
 */
function app_get_img ($img, $path, $width, $height)
{
    $CI =& get_instance();
    $CI->load->library("Upload");

    $path_com_tamanho = "{$path}/{$width}_{$height}";
    $return_path = FCPATH . "assets/upload/{$path_com_tamanho}/";
    if(app_image_exists($path_com_tamanho.'/', $img))
    {
        return $return_path . $img;
    }
    else if (app_image_exists("{$path}", $img))
    {


        return false;
    }
    else
    {

        if(is_null($img) || $img == "null" || empty($img))
        {
            return FCPATH . "assets/admin/core/images/no-image.jpg";
        }
        else
        {

            $thumb = $CI->upload->create_thumbnail(FCPATH . "assets/upload/{$path}/", $img, $width, $height);

            if(!$thumb)
            {
                exit ("Não criou a thumb.");
            }

            return $return_path . $img;
        }


    }
}

/**
 * Verifica permissão
 * @param string $action
 * @param null $resource
 * @return mixed
 */
function app_check_permission($action = 'view', $resource = null){
    $CI =& get_instance();

    return $CI->auth->check_permission($action, $resource);

}

/**
 * Seleciona título por class
 * @return string
 */
function app_get_breadcrumb($classe)
{
    $CI =& get_instance();

    $CI->load->model("Usuario_acl_recurso_model", "usuario_acl_recurso");
    $recurso = $CI->usuario_acl_recurso->get_by(array(
        'controller' => $classe
    ));

    $html = "";
    if($recurso)
    {
        $recurso_pai = $CI->usuario_acl_recurso->get($recurso['pai_id']);

        $html = "<a href='".admin_url($recurso['controller'])."'>{$recurso['nome']}</a>";

        if($recurso_pai)
        {
            $html =  "<a href='".admin_url($recurso_pai['controller'])."'>{$recurso_pai['nome']}</a> > " . $html;
        }
    }

    return $html;
}

function app_get_mysql_operator($op = '')
{
    if($op == "startswith")
        return array(
            'operator' => 'LIKE',
            'before' => '',
            'after' => '%'
        );
    else if(strtolower($op) == "like")
    {
        return array(
            'operator' => 'LIKE',
            'before' => '%',
            'after' => '%'
        );
    }
    else if(strtolower($op) == "in")
    {
        return array(
            'operator' => 'IN',
            'before' => '(',
            'after' => ')'
        );
    }
    else
        return array(
            'operator' => '=',
            'before' => '',
            'after' => ''
        );
}

function app_link_img($img, $w, $h)
{
    return base_url("services/Image/get_image/padrao/$img/$w/$h");
}

function app_date_only_javascript_to_mysql($value)
{
    if(strpos($value, '(') !== FALSE){
        $value = substr($value, 0, strpos($value, '('));
        $date = new DateTime($value);
        return $date->format('Y-m-d');
    }else{
        return "";
    }

}
/**
 * Requere um diretório
 * @param $dir
 */
function require_once_dir($dir){

    $item = glob($dir);

    foreach ($item as $filename) {

        if(is_dir($filename)) {

            require_once_dir($filename.'/'. "*");

        }
        elseif(is_file($filename))
        {
            include_once($filename);
        }
    }
}

/**
 * Retorna promessa ID
 * @return string
 */
function app_get_promise()
{
    return date('mdhi') . rand(1,10);
}

/**
 * Slugfica
 * @param $string
 * @return mixed|string
 */
function app_slugify($string)
{

    $table = array(
        'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z',
        'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
        'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
        'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
        'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
        'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
        'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
        'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
        'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
        'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
        'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
        'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
        'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
        'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
        'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
    );
    // Traduz os caracteres em $string, baseado no vetor $table
    $string = strtr($string, $table);
    // converte para minúsculo
    $string = strtolower($string);
    // remove caracteres indesejáveis (que não estão no padrão)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // Remove múltiplas ocorrências de hífens ou espaços
    $string = preg_replace("/[\s-]+/", " ", $string);
    // Transforma espaços e underscores em hífens
    $string = preg_replace("/[\s_]/", "_", $string);
    // retorna a string
    return $string;
}

function app_get_configuracao($key){

    $CI =& get_instance();
    $CI->load->model('Configuracao_model', 'configuracao_model');

    return $CI->configuracao_model->get_config($key);
}

function app_get_extension_filename($filename, $file_ext_tolower = true)
{
    $x = explode('.', $filename);

    if (count($x) === 1)
    {
        return '';
    }

    $ext = ($file_ext_tolower) ? strtolower(end($x)) : end($x);
    return '.'.$ext;
}