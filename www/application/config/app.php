<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
|  Assets directory
|--------------------------------------------------------------------------
|
| Assets directory
|
*/
$config['app_assets_dir'] = 'assets';

if(ENVIRONMENT == 'development')
    $config['app_assets_ver'] = date('ymdhis');
else
    $config['app_assets_ver'] = '1.0.0.0';

$config['app_assets_version_name'] = "version";

