<?php

$config['events'] = array();
$config['event_path'] = 'events';
$config['event_prefix'] = '';

/**
 * Event: after_save_contrato_associado
 * Info: Chamado apos insert/delete dos dados do contrato_associado
 * Params:
 *          $data Dado que serão inseridos ou atualizados
 *          $contrato_associado_id id do contrato associado
 */
//$config['events']['after.save_contrato_associado'][] = array('Dr_Particular', 'after_save_contrato_associado');