<?php

class MY_Migration extends CI_Migration
{
    protected $timestamps = array(
        "alterado_em TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Data da alteração do registro'",
        /*'alterado_em' => array(
            'type' => 'TIMESTAMP',
            'default' => 'CURRENT_TIMESTAMP',
            'default_string' => false,
            'null' => true,
        ), */
        'criado_em' => array(
            'type' => 'TIMESTAMP',
            'null' => true,
        ),
        'permite_editar' => array(
            'type' => 'TINYINT',
            'contraint' => "1",
            "default" => 1,
        ),
        'permite_excluir' => array(
            'type' => 'TINYINT',
            'contraint' => "1",
            "default" => 1,
        ),
        'excluido' => array(
            'type' => 'TINYINT',
            'constraint' => '1',
            'default' => 0,
        ),
    );


    public function add_foreign_key($table, $foreign_table, $table_field = '')
    {
        if(!$table_field)
            $table_field = $foreign_table . '_id';

        $this->db->query('ALTER TABLE '.$table.' ADD CONSTRAINT FK_'.$table.'_'.$foreign_table.' FOREIGN KEY ('.$table_field.') REFERENCES '.$foreign_table.'('.$table_field.');');
    }


}