<?php

class MY_Controller extends CI_Controller
{


    /**
     * Retorna metodo atual
     * @param bool $lower
     * @return string
     */
    protected function get_method($lower = true)
    {
        $method = app_current_method();

        if($lower)
            return strtolower($method);
        return $method;
    }

    /**
     * Retorna classe atual
     * @param bool $lower
     * @return string
     */
    protected function get_class($lower = true)
    {
        $controller = app_current_controller();
        if($lower)
            return strtolower($controller);
        return $controller;
    }

}