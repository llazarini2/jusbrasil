<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Api_Controller
 */
class Test_Controller extends MY_Controller
{
    /**
     * Api_Controller Constructor.
     */
    function __construct()
    {

        parent::__construct();

        $this->load->library("unit_test");

    }


}