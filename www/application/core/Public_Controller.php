<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Public_Controller
 */
class Public_Controller extends MY_Controller
{
    /**
     * Public_Controller constructor.
     */
    function __construct()
    {
        parent::__construct();

        //Carrega JS da página no template automaticamente
        $this->autoload_js();

    }

    /**
     * Carrega JS da página correspondente
     */
    public function autoload_js()
    {
        $controller = strtolower(app_current_controller());
        $method = strtolower(app_current_method());


        if($method == "add" || $method == "edit")
            $method = "add_edit";

        if(file_exists(FCPATH . "assets/site/modulos/{$controller}/js/{$method}.js"))
        {

            $js_version = rand(1,1000) . date("YmdHis");
            $this->template->js(app_assets_url("modulos/{$controller}/js/{$method}.js?v={$js_version}","site"));
        }
    }


}