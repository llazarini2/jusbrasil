<?php
/**
 * A base model with a series of CRUD functions (powered by CI's query builder),
 * validation-in-model support, event callbacks and more.
 *
 * @link http://github.com/jamierumbelow/codeigniter-base-model
 * @copyright Copyright (c) 2012, Jamie Rumbelow <http://jamierumbelow.net>
 */

class MY_Model extends CI_Model
{

    /* --------------------------------------------------------------
     * VARIABLES
     * ------------------------------------------------------------ */

    /**
     * This model's default database table. Automatically
     * guessed by pluralising the model name.
     */
    protected $_table;

    public $_db = 'sistema';

    /**
     * The database connection object. Will be set to the default
     * connection. This allows individual models to use different DBs
     * without overwriting CI's global $this->db connection.
     */
    protected $_database;

    /**
     * This model's default primary key or unique identifier.
     * Used by the get(), update() and delete() functions.
     */
    protected $primary_key = 'id';

    /**
     * Support for soft deletes and this model's 'deleted' key
     */
    protected $_temporary_with_deleted = FALSE;
    protected $_temporary_only_deleted = FALSE;

    protected $controle_excluir = TRUE;
    protected $controle_excluir_campo = "permite_excluir";

    protected $controle_editar = TRUE;
    protected $controle_editar_campo = "permite_editar";

    /**
     * The various callbacks available to the model. Each are
     * simple lists of method names (methods will be run on $this).
     */

    protected $before_create = array('create_timestamps' );
    protected $after_create = array('run_submodels');
    protected $before_update = array('update_timestamps');
    protected $after_update = array('run_submodels');
    protected $before_get = array();
    protected $after_get = array();
    protected $before_delete = array();
    protected $after_delete = array();
    protected $limit = null;
    protected $offset = null;
    protected $upload_path = "padrao";

    protected $callback_parameters = array();

    protected $files = array();
    /**
     * Protected, non-modifiable attributes
     */
    protected $protected_attributes = array();

    /**
     * Relationship arrays. Use flat strings for defaults or string
     * => array to customise the class name and primary key
     */
    protected $belongs_to = array();
    protected $has_many = array();

    protected $_with = array();

    /**
     * An array of validation rules. This needs to be the same format
     * as validation rules passed to the Form_validation library.
     */
    protected $validate_groups = array();
    protected $enable_log = FALSE;

    public function reset_joins()
    {
        return $this;
    }

    /**
     *  Example array
     *
     *  public $validate = array(
            array(
                'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email|is_unique[users.email]',
                'groups' => 'default, backend, frontend'
            ),
            array(
                    'field' => 'password',
                    'label' => 'password',
                    'rules' => 'required'
                ),
            array(
                'field' => 'password_confirmation',
                'label' => 'confirm password',
                'rules' => 'required|matches[password]'
            )
        );
     */


    protected $return_type = 'array';
    protected $soft_delete = TRUE;

    protected $soft_delete_key = 'excluido';
    protected $update_at_key = 'alterado_em';
    protected $create_at_key = 'criado_em';


    /**
     * Optionally skip the validation. Used in conjunction with
     * skip_validation() to skip data validation for any future calls.
     */
    protected $skip_validation = FALSE;

    /**
     * By default we return our results as objects. If we need to override
     * this, we can, or, we could use the `as_array()` and `as_object()` scopes.
     */
    protected $_temporary_return_type = NULL;


    protected $obj_count_foreign = 0; //Contador

    //config Kendo
    public $fields = array();



    /* --------------------------------------------------------------
     * GENERIC METHODS
     * ------------------------------------------------------------ */

    /**
     * Initialise the model, tie into the CodeIgniter superobject and
     * try our best to guess the table name.
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->helper('inflector');

        $this->_fetch_table();

        $this->_build_validation_groups();


        if(is_null($this->_db))
        {
            $this->_database = $this->db;
        }
        else
        {
            $this->_database = $this->load->database($this->_db ,true);
        }

        $this->_database = $this->load->database($this->_db ,true);

        array_unshift($this->before_create, 'protect_attributes');
        array_unshift($this->before_update, 'protect_attributes');

        $this->_temporary_return_type = $this->return_type;

        //$this->setPermissoes();

    }



    /* --------------------------------------------------------------
     * CRUD INTERFACE
     * ------------------------------------------------------------ */

    /**
     * Fetch a single record based on the primary key. Returns an object.
     */
    public function get($primary_value, $selectAll = false)
    {
            if($selectAll)
                $this->_database->select($this->_table.'.*');

            return $this->get_by($this->_table . "." . $this->primary_key, $primary_value);
    }


    /**
     * Fetch a single record based on an arbitrary WHERE call. Can be
     * any valid value to $this->_database->where().
     */
    public function get_by()
    {
        $where = func_get_args();
        $this->_database->select($this->_table.'.*');

        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where("{$this->_table}.{$this->soft_delete_key}", (bool)$this->_temporary_only_deleted);
        }

        $this->_set_where($where);

        $this->trigger('before_get');

        $row = $this->_database->get($this->_table)
            ->{$this->_return_type()}();
        $this->_temporary_return_type = $this->return_type;

        $row = $this->trigger('after_get', $row);

        $this->_with = array();
        return $row;
    }

    /**
     * Fetch an array of records based on an array of primary values.
     */
    public function get_many($values)
    {
        $this->_database->where_in($this->_table.'.'.$this->primary_key, $values);

        return $this->get_all();
    }


    /**
     * Seleciona um array
     */
    public function get_many_by()
    {
        $where = func_get_args();
        $this->_set_where($where);

        return $this->get_all();
    }


    /**
     * Fetch all the records in the table. Can be used as a generic call
     * to $this->_database->get() with scoped methods.
     */
    public function get_all($limit = 0, $offset = 0)
    {
        $this->trigger('before_get');


        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where("{$this->_table}.{$this->soft_delete_key}", (bool)$this->_temporary_only_deleted);
        }

        $this->_database->select($this->_table . '.*');

        $result = $this->_database->get($this->_table)
                                  ->{$this->_return_type(1)}();
        $this->_temporary_return_type = $this->return_type;

        foreach ($result as $key => &$row)
        {
            if(!empty($this->files))
            {
                $this->load->model("Arquivo_model","arquivo_model");

                foreach($this->files as $file_name)
                {
                    $row[$file_name] = $this->arquivo_model->arquivo(ucfirst($this->_table), $file_name, $row[$this->primary_key]);
                }

            }

            $row = $this->trigger('after_get', $row, ($key == count($result) - 1));
        }

        $this->_with = array();
        return $result;
    }
    /**
     * Insert a new row into the table. $data should be an associative array
     * of data to be inserted. Returns newly created ID.
     */
    public function insert($data, $skip_validation = FALSE)
    {
        if ($skip_validation === FALSE)
        {
            $data = $this->get_form_data(false, $data);
        }

        if ($data !== FALSE)
        {
            $data = $this->trigger('before_create', $data);

            $this->_database->insert($this->_table, $data);
            $insert_id = $this->_database->insert_id();
            /**
             * Log event insert
             */
            if($this->enable_log){
                $this->log_evento->log_inserir($this , $insert_id, $data);
            }

            $this->trigger('after_create', $insert_id);

            return $insert_id;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Insert multiple rows into the table. Returns an array of multiple IDs.
     */
    public function insert_many($data, $skip_validation = FALSE)
    {
        $ids = array();

        foreach ($data as $key => $row)
        {
            $ids[] = $this->insert($row, $skip_validation, ($key == count($data) - 1));
        }

        /**
         * Log event insert
         */
        foreach($ids as $id){

            if($this->enable_log){
                $this->log_evento->log_inserir($this , $id, $data);
            }

        }

        return $ids;
    }

    /**
     * Atualiza um registro
     * @param $primary_value
     * @param $data
     * @param bool $skip_validation
     * @return bool
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $data = $this->trigger('before_update', $data);

        if ($skip_validation === FALSE)
        {
            $data = $this->validate($data);
        }

        if ($data !== FALSE)
        {
            //Efetua LOG
            if($this->enable_log){
                $this->log_evento->log_alterar($this , $this->get($primary_value), $data);
            }

            if($this->controle_editar)
            {
                $result = $this->_database
                    ->where($this->controle_editar_campo, 1)
                    ->where($this->primary_key, $primary_value)
                    ->set($data)
                    ->update($this->_table);
            }
            else
            {
                $result = $this->_database
                    ->where($this->primary_key, $primary_value)
                    ->set($data)
                    ->update($this->_table);
            }

            $this->trigger('after_update', array($data, $result));


            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Update many records, based on an array of primary values.
     */
    public function update_many_where($condicao, $data, $skip_validation = FALSE)
    {
        if($this->controle_editar)
        {
            $many = $this
                ->get_many_by($condicao);
        }


        $ids = array();
        foreach($many as $m)
        {
            $ids[] = $m[$this->primary_key];
        }

        if($ids)
            return $this->update_many($ids,  $data, $skip_validation);
        return true;
    }
    /**
     * Update many records, based on an array of primary values.
     */
    public function update_many($primary_values, $data, $skip_validation = FALSE)
    {
        $data = $this->trigger('before_update', $data);

        if ($skip_validation === FALSE)
        {
            $data = $this->validate($data);
        }

        if ($data !== FALSE)
        {

            /**
             * Log update
             */
            if($this->enable_log){
                foreach($primary_values as $primary_id){

                    $this->log_evento->log_alterar($this , $this->get($primary_id), $data);
                }
            }

            $result = $this->_database->where_in($this->primary_key, $primary_values)
                ->set($data)
                ->update($this->_table);



            $this->trigger('after_update', array($data, $result));

            return $result;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     * Updated a record based on an arbitrary WHERE clause.
     */
    public function update_by()
    {
        $args = func_get_args();
        $data = array_pop($args);

        $data = $this->trigger('before_update', $data);

        if ($this->validate($data) !== FALSE)
        {
            $this->_set_where($args);
            $result = $this->_database->set($data)
                ->update($this->_table);
            $this->trigger('after_update', array($data, $result));

            return $result;
        }
        else
        {
            return FALSE;
        }
    }


    /**
     * Delete a row from the table by the primary value
     */
    public function delete($id)
    {
        $this->trigger('before_delete', $id);

        $this->_database->where($this->primary_key, $id);

        //Verifica se log está ligado
        if($this->enable_log)
        {
            $this->log_evento->log_excluir($this , $id, $this->get($id));
        }

        if ($this->soft_delete)
        {
            $result = $this->_database
                ->where($this->primary_key, $id);

            //Verifica controle excluir
            if($this->controle_excluir)
                $result->where($this->controle_excluir_campo, 1);

            $result->update($this->_table, array( $this->soft_delete_key => TRUE ));
        }
        else
        {
            $result = $this->_database
                ->where($this->primary_key, $id);

            //Verifica controle excluir
            if($this->controle_excluir)
                $result->where($this->controle_excluir_campo, 1);

            $result->delete($this->_table);
        }

        $this->trigger('after_delete', $result);

        return $result;
    }


    /**
     * Delete a row from the database table by an arbitrary WHERE clause
     */
    public function delete_by()
    {
        $where = func_get_args();


        $where = $this->trigger('before_delete', $where);
        if($where)
        {
            $this->_set_where($where);
        }

        if ($this->soft_delete)
        {
            //Verifica controle excluir
            if($this->controle_excluir)
                $result = $this->_database
                    ->where($this->controle_excluir_campo, 1)
                    ->update($this->_table, array( $this->soft_delete_key => TRUE ));
            else
                $result = $this->_database->update($this->_table, array( $this->soft_delete_key => TRUE ));
        }
        else
        {
            //Verifica controle excluir
            if($this->controle_excluir)
                $result = $this->_database
                    ->where($this->controle_excluir_campo, 1)
                    ->delete($this->_table);
            else
                $result = $this->_database->delete($this->_table);
        }

        $this->trigger('after_delete', $result);

        return $result;
    }

    /**
     * Delete many rows from the database table by multiple primary values
     */
    public function delete_many($primary_values)
    {
        $primary_values = $this->trigger('before_delete', $primary_values);

        /**
         * log delete event
         */
        foreach($primary_values as $primary_id){

            $this->log_evento->log_excluir($this , $primary_id, $this->get($primary_id));
        }

        $this->_database->where_in($this->primary_key, $primary_values);

        if ($this->soft_delete)
        {
            if($this->controle_excluir)
            {
                $result = $this->_database
                    ->where($this->controle_excluir_campo, 1)
                    ->update($this->_table, array( $this->soft_delete_key => TRUE ));
            }
            else
            {
                $result = $this->_database
                    ->update($this->_table, array( $this->soft_delete_key => TRUE ));
            }
        }
        else
        {
            if($this->controle_excluir)
            {
                $result = $this->_database
                    ->where($this->controle_excluir_campo, 1)
                    ->delete($this->_table);
            }
            else
            {
                $result = $this->_database
                    ->delete($this->_table);
            }
        }

        $this->trigger('after_delete', $result);

        return $result;
    }


    /**
     * Truncates the table
     */
    public function truncate()
    {
        $result = $this->_database->truncate($this->_table);

        return $result;
    }


    /* --------------------------------------------------------------
     * UTILITY METHODS
     * ------------------------------------------------------------ */


    /**
     * Fetch a total count of rows, disregarding any previous conditions
     */
    public function count_all()
    {
        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where("{$this->_table}.{$this->soft_delete_key}", (bool)$this->_temporary_only_deleted);
        }

        return $this->_database->count_all($this->_table);
    }

    function getTotal()
    {
        if ($this->soft_delete && $this->_temporary_with_deleted !== TRUE)
        {
            $this->_database->where("{$this->_table}.{$this->soft_delete_key}", (bool)$this->_temporary_only_deleted);
        }
        $result =  $this->_database->count_all_results($this->_table);


        return $result;

    }

    /**
     * Tell the class to skip the insert validation
     */
    public function skip_validation()
    {
        $this->skip_validation = TRUE;
        return $this;
    }

    /**
     * Get the skip validation status
     */
    public function get_skip_validation()
    {
        return $this->skip_validation;
    }

    /**
     * Return the next auto increment of the table. Only tested on MySQL.
     */
    public function get_next_id()
    {
        return (int) $this->_database->select('AUTO_INCREMENT')
            ->from('information_schema.TABLES')
            ->where('TABLE_NAME', $this->_table)
            ->where('TABLE_SCHEMA', $this->_database->database)->get()->row()->AUTO_INCREMENT;
    }

    /**
     * Getter for the table name
     */
    public function table()
    {
        return $this->_table;
    }

    /* --------------------------------------------------------------
     * GLOBAL SCOPES
     * ------------------------------------------------------------ */

    /**
     * Return the next call as an array rather than an object
     */
    public function as_array()
    {
        $this->_temporary_return_type = 'array';
        return $this;
    }

    /**
     * Return the next call as an object rather than an array
     */
    public function as_object()
    {
        $this->_temporary_return_type = 'object';
        return $this;
    }

    /**
     * Don't care about soft deleted rows on the next call
     */
    public function with_deleted()
    {
        $this->_temporary_with_deleted = TRUE;
        return $this;
    }

    /**
     * Only get deleted rows on the next call
     */
    public function only_deleted()
    {
        $this->_temporary_only_deleted = TRUE;
        return $this;
    }

    /* --------------------------------------------------------------
     * OBSERVERS
     * ------------------------------------------------------------ */

    /**
     * MySQL DATETIME created_at and updated_at
     */
    public function created_at($row)
    {
        if (is_object($row))
        {
            $row->created_at = date('Y-m-d H:i:s');
        }
        else
        {
            $row['created_at'] = date('Y-m-d H:i:s');
        }

        return $row;
    }

    public function updated_at($row)
    {
        if (is_object($row))
        {
            $row->updated_at = date('Y-m-d H:i:s');
        }
        else
        {
            $row['updated_at'] = date('Y-m-d H:i:s');
        }

        return $row;
    }

    /**
     * Serialises data for you automatically, allowing you to pass
     * through objects and let it handle the serialisation in the background
     */
    public function serialize($row)
    {
        foreach ($this->callback_parameters as $column)
        {
            $row[$column] = serialize($row[$column]);
        }

        return $row;
    }

    public function unserialize($row)
    {
        foreach ($this->callback_parameters as $column)
        {
            if (is_array($row))
            {
                $row[$column] = unserialize($row[$column]);
            }
            else
            {
                $row->$column = unserialize($row->$column);
            }
        }

        return $row;
    }

    /**
     * Protect attributes by removing them from $row array
     */
    public function protect_attributes($row)
    {
        foreach ($this->protected_attributes as $attr)
        {
            if (is_object($row))
            {
                unset($row->$attr);
            }
            else
            {
                unset($row[$attr]);
            }
        }

        return $row;
    }

    /* --------------------------------------------------------------
     * QUERY BUILDER DIRECT ACCESS METHODS
     * ------------------------------------------------------------ */

    /**
     * A wrapper to $this->_database->order_by()
     */
    public function order_by($criteria, $order = 'ASC')
    {
        if ( is_array($criteria) )
        {
            foreach ($criteria as $key => $value)
            {
                $this->_database->order_by($key, $value);
            }
        }
        else
        {
            $this->_database->order_by($criteria, $order);
        }
        return $this;
    }

    /**
     * A wrapper to $this->_database->limit()
     */
    public function limit($limit, $offset = 0)
    {
        $this->limit = $limit;
        $this->offset = $offset;
        $this->_database->limit($limit, $offset);
        return $this;
    }

    /* --------------------------------------------------------------
     * INTERNAL METHODS
     * ------------------------------------------------------------ */

    /**
     * Trigger an event and call its observers. Pass through the event name
     * (which looks for an instance variable $this->event_name), an array of
     * parameters to pass through and an optional 'last in interation' boolean
     */
    public function trigger($event, $data = FALSE, $last = TRUE)
    {
        if (isset($this->$event) && is_array($this->$event))
        {
            foreach ($this->$event as $method)
            {
                if (strpos($method, '('))
                {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }

                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }

        return $data;
    }

    /**
     * Run validation on the passed data
     */
    public function validate($data, $group = 'default')
    {
        if($this->skip_validation)
        {
            return $data;
        }

        if(isset($this->validate_groups[$group]) && !empty($this->fields))
        {
            foreach($data as $key => $val)
            {
                $_POST[$key] = $val;
            }



            $this->load->library('form_validation');

            if(is_array($this->validate_groups[$group]))
            {
                $this->form_validation->set_rules($this->validate_groups[$group]);

                if ($this->form_validation->run() === TRUE)
                {
                    return $data;
                }
                else
                {
                    return FALSE;
                }
            }
            else
            {

                /**
                 * @todo Verificar necessidade dessa linha
                 */
                if ($this->form_validation->run($this->validate_groups[$group]) === TRUE)
                {
                    return $data;
                }
                else
                {
                    return FALSE;
                }
            }
        }
        else
        {
            return $data;
        }
    }

    /**
     * Guess the table name by pluralising the model name
     */
    private function _fetch_table()
    {
        if ($this->_table == NULL)
        {
            $this->_table = plural(preg_replace('/(_m|_model)?$/', '', strtolower(get_class($this))));
        }
    }

    /**
     * Guess the primary key for current table
     */
    private function _fetch_primary_key()
    {
        if($this->primary_key == NULl)
        {
            $this->primary_key = $this->_database->query("SHOW KEYS FROM `".$this->_table."` WHERE Key_name = 'PRIMARY'")->row()->Column_name;
        }
    }

    /**
     * Set WHERE parameters, cleverly
     */
    protected function _set_where($params)
    {
        if (count($params) == 1 && is_array($params[0]))
        {
            foreach ($params[0] as $field => $filter)
            {
                if (is_array($filter))
                {
                    $this->_database->where_in($field, $filter);
                }
                else
                {
                    if (is_int($field))
                    {
                        $this->_database->where($filter);
                    }
                    else
                    {
                        $this->_database->where($field, $filter);
                    }
                }
            }
        }
        else if (count($params) == 1)
        {
            $this->_database->where($params[0]);
        }
        else if(count($params) == 2)
        {
            if (is_array($params[1]))
            {
                $this->_database->where_in($params[0], $params[1]);
            }
            else
            {
                $this->_database->where($params[0], $params[1]);
            }
        }
        else if(count($params) == 3)
        {
            $this->_database->where($params[0], $params[1], $params[2]);
        }
        else
        {
            if (is_array($params[1]))
            {
                $this->_database->where_in($params[0], $params[1]);
            }
            else
            {
                $this->_database->where($params[0], $params[1]);
            }
        }
    }

    /**
     * Return the method name for the current return type
     */
    protected function _return_type($multi = FALSE)
    {
        $method = ($multi) ? 'result' : 'row';
        return $this->_temporary_return_type == 'array' ? $method . '_array' : $method;
    }

    private function _build_validation_groups(){

        if(!empty($this->fields))
        {

            foreach($this->fields as $field)
            {
                if(isset($field['groups'])){

                    $groups = explode('|', $field['groups']);

                    unset($field['groups']);

                    foreach($groups as $group){

                        $group = trim($group);

                        $this->validate_groups[$group][] = $field;
                    }

                }else {

                    $this->validate_groups['default'][] = $field;
                }
            }

        }

    }

    /**
     * Retorna chave primária
     * @return string
     */
    public function primary_key (){

        return $this->primary_key;
    }

    protected function update_timestamps($row){

        $row[$this->update_at_key] = date('Y-m-d H:i:s');
        return $row;
    }


    protected function create_timestamps($row)
    {
        $row[$this->create_at_key] = $row[$this->update_at_key] = date('Y-m-d H:i:s');
        return $row;
    }

    /**
     * Valida formulário POST
     * @param string $group
     * @return bool
     */
    public function validate_form($group = 'default')
    {
        $data = $this->get_form_data(true);

        return $this->validate($data, $group);
    }


    /**
     * Retorna dados do formulário
     * @return array
     */
    public function get_form_data($just_check = false, $from_post_array = '')
    {
        $data = array();

        $prefix = "";
        $posfix = "";

        if(!empty($from_post_array) && !is_array($from_post_array))
        {
            $prefix = $from_post_array . "[";
            $posfix = "]";
        }


        //Para cada dado
        foreach($this->fields as $dado)
        {

            //Checa se o dado foi inserido
            if(!isset($dado['ignore']) && !isset($dado['type']))
            {
                //Seta dado
                if (is_array($from_post_array))
                    $data[$dado['field']] = issetor($from_post_array[$dado['field']]);
                else
                    $data[$dado['field']] = $this->input->post($prefix . $dado['field'] . $posfix);
            }
            else if(isset($dado['type']))
            {
                //Se for um tipo arquivo
                if($dado['type'] == "file")
                {
                    if(!$just_check) //Caso for apenas para checar se é válido (Não realiza update)
                    {
                        if(!$this->input->post("sem_arquivo"))
                        {
                            //Resgata arquivo
                            $file = $this->do_upload($dado['field'], $this->upload_path);

                            //Se existir o arquivo
                            if(!is_null($file) && $file != false)
                            {
                                $data[$dado['field']] = $file; //Seta arquivo
                            }
                        }
                        else
                        {
                            $data[$dado['field']] = ""; //Seta arquivo
                        }

                    }
                }

                //Se for do tipo data
                else if ($dado['type'] == 'date')
                {
                    if (is_array($from_post_array))
                        $data[$dado['field']] = app_date_mask_to_mysql(issetor($from_post_array[$dado['field']]));
                    else
                        $data[$dado['field']] = app_date_mask_to_mysql($this->input->post($prefix . $dado['field'] . $posfix).' 00:00:00');
                }

                //Se for campo ativo apenas resgata valor
                else if($dado['type'] == 'active')
                {
                    if (is_array($from_post_array))
                        $data[$dado['field']] = (issetor($from_post_array[$dado['field']]));
                    else
                        $data[$dado['field']] = $this->input->post($prefix . $dado['field'] . $posfix);
                }

                //Se for campo ativo apenas resgata valor
                else if($dado['type'] == 'money')
                {
                    if (is_array($from_post_array))
                        $valor = (issetor($from_post_array[$dado['field']]));
                    else
                        $valor = $this->input->post($prefix . $dado['field'] . $posfix);

                    $data[$dado['field']] = app_unformat_currency($valor);
                }

                //Se for campo ativo apenas resgata valor
                else if($dado['type'] == 'number')
                {
                    if (is_array($from_post_array))
                        $valor = (issetor($from_post_array[$dado['field']]));
                    else
                        $valor = $this->input->post($prefix . $dado['field'] . $posfix);

                    $data[$dado['field']] = $valor;
                }

                //Se for array
                else if ($dado['type'] == 'array')
                {
                    if (is_array($from_post_array))
                        $dados = (issetor($from_post_array[$dado['field']]));
                    else
                        $dados = $this->input->post($prefix . $dado['field'] . $posfix);

                    if(is_array($dados))
                    {
                        $data[$dado['field']] = "";
                        foreach ($dados as $value)
                        {
                            $data[$dado['field']] .= "{$value},";
                        }
                    }

                }
                else
                {

                    if (is_array($from_post_array))
                        $valor = (issetor($from_post_array[$dado['field']]));
                    else
                        $valor = $this->input->post($prefix . $dado['field'] . $posfix);

                    $data[$dado['field']] = $valor;
                }
            }
        }


        return $data;
    }

    /*
     * Insere form
     */
    public function insert_form()
    {
        $data = $this->get_form_data();
        return $this->insert($data, true);
    }

    /**
     * Realiza update na form
     */
    public function update_form()
    {
        $data = $this->get_form_data();
        return $this->update($this->input->post($this->primary_key), $data, true);
    }


    /**
     * Relação JOIN simples
     * @param $with_table
     * @param $prefix
     * @param $foreing_key
     * @param $fields
     * @param string $join
     * @return $this
     */
    public function with_simple_relation($with_table, $prefix , $foreing_key , $fields, $join = 'inner'){


        if(is_array($with_table)){

        }


        foreach($fields as $field){

            $this->_database->select("{$with_table}.$field AS {$prefix}{$field}");
        }

        $this->_database->join($with_table, $this->_table.".{$foreing_key} = {$with_table}.{$foreing_key}", $join);

        return $this;

    }

    /**
     * Relação Join Simples
     * @param $with_table
     * @param $prefix
     * @param $foreing_key1
     * @param $foreign_key2
     * @param $fields
     * @param string $join
     * @return $this
     */
    public function with_simple_relation_foreign($with_table, $prefix , $foreing_key1, $foreign_key2 , $fields, $join = 'inner'){

        if(is_array($with_table)){
        }

        foreach($fields as $field){

            $this->_database->select("{$with_table}.$field AS {$prefix}{$field}");
        }
        $this->_database->join($with_table, $this->_table.".{$foreing_key1} = {$with_table}.{$foreign_key2}", $join);

        return $this;
    }

    /**
     * Seta parametro
     * @param $campo
     * @param $operacao
     * @param $parametro
     * @return $this
     */
    public function where($campo, $operacao, $parametro)
    {

        $bf = "'";
        $af = "'";

        if(strtolower(trim($operacao)) == "in")
        {
            $bf = "";
            $af = "";
        }

        $this->_database->where("{$campo} {$operacao} {$bf}{$parametro}{$af}");
        return $this;
    }

    public function or_where($campo, $operacao, $parametro)
    {
        $this->_database->or_where("{$campo} {$operacao} '{$parametro}'");
        return $this;
    }

    public function full_where($where)
    {
        $this->_database->where($where);
        return $this;
    }


    /**
     * Busca campos estrangeiros automaticamente
     * @param array $campos
     * @return $this
     */
    public function with_foreign($campos = array())
    {
        //Get data
        $data = $this->fields;

        $campo_tabela = array();

        //Para cada campo
        foreach($data as $value)
        {
            //Checa se possui propriedade foreign_table
            if(isset($value['foreign']))
            {

                //Carrega model
                $this->load->model("{$value['foreign']}_model", "{$value['foreign']}_model");

                //Carrega campo
                $campo_tabela = array_keys($this->{$value['foreign'] . '_model'}->get_form_data(true));
                $join = (isset($value['foreign_join'])) ? $value['foreign_join'] : null;
                $foreign_key = (isset($value['foreign_key'])) ? $value['foreign_key'] : "{$value['foreign']}_id";


                //Realiza relação simples
                $this->with_simple_relation_foreign($value['foreign'], "{$value['foreign']}_", $value['field'], "$foreign_key", $campo_tabela, $join);

            }
            $this->obj_count_foreign++;
        }
        return $this;
    }


    /**
     * Possui campo
     * @param $field
     * @return bool
     */
    public function has_field($field)
    {
        foreach($this->fields as $f)
        {
            if($f['field'] == $field)
                return true;
        }

        return false;
    }

    /**
     * Retorna campo
     * @param $field
     * @return bool|mixed
     */
    public function get_field($field)
    {
        foreach($this->fields as $f)
        {
            if($f['field'] == $field)
                return $f;
        }

        return false;
    }

    /**
     * Seta parametro Kendo
     * @param $field
     * @param $operator
     * @param $value
     * @return $this
     */
    public function set_operator($field, $operator, $value, $logic = 'and')
    {
        $field_obj = $this->get_field($field);

        $is_date = FALSE;
        if(isset($field_obj['type']) && $field_obj['type'] == 'date'){
            $value = app_date_only_javascript_to_mysql($value);
            $is_date = TRUE;
        }

        $field = isset($field_obj['field_filter']) ? "{$field_obj['field_filter']}" : $field;

        $where = ($logic == 'and') ? 'where' : 'or_where';

        switch ($operator)
        {
            case 'eq': //IGUAL
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} BETWEEN '{$value} 00:00:00' AND '{$value} 23:59:59'");
                }else{

                    $this->_database->$where("{$field} = ", $value);
                }

                break;
            case 'neq': // não é igual
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} NOT BETWEEN '{$value} 00:00:00' AND '{$value} 23:59:59'", NULL, FALSE);
                }else {
                    $this->_database->$where("{$field} NOT LIKE ", $value);
                }
                break;
            case 'startswith':
                $this->_database->$where("{$field} LIKE ", "{$value}%");
                break;
            case 'contains':
                $this->_database->$where("{$field} LIKE ", "%{$value}%");
                break;
            case 'doesnotcontain':
                $this->_database->$where("{$field} NOT LIKE ", "%{$value}%");
                break;
            case 'endswith':
                $this->_database->$where("{$field} LIKE ", "%{$value}");
                break;
            case 'gte':
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} >= '{$value} 00:00:00'");
                }else{
                    $this->_database->$where("{$field} >= ", $value);
                }
                break;
            case 'gt':
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} > '{$value} 23:59:59'");
                }else{
                    $this->_database->$where("{$field} > ", $value);
                }
                break;
            case 'lte':
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} <= '{$value} 23:59:59'");
                }else{
                    $this->_database->$where("{$field} <= ", $value);
                }
                break;
            case 'lt':
                if($is_date){
                    //exit("{$field} BETWEEN ('{$value} 00:00:00' AND '{$value} 23:59:59')");
                    $this->_database->$where("{$field} < '{$value} 00:00:00'");
                }else{
                    $this->_database->$where("{$field} < ", $value);
                }
                break;
            case 'isnotempty':
                $this->_database->$where("{$field} <> ", "");
                break;
            case 'isempty':
                $this->_database->$where("{$field} = ", "");
                break;
            case 'isnotnull':
                $this->_database->$where("{$field} IS NOT NULL ", NULL, FALSE);
                break;
            case 'isnull':
                $this->_database->$where("{$field} IS NULL ", NULL, FALSE);
                break;
        }

        return $this;
    }

    /**
     * Começo de Transação
     * @return $this
     */
    public function trans_start()
    {
        $this->_database->trans_start();
        return $this;
    }

    /**
     * Transação completa
     * @return $this
     */
    public function trans_complete()
    {
        $this->_database->trans_complete();
        return $this;
    }

    /**
     * Atualiza fields com type "Submodel", inserindo nos respectivos models
     */
    public function run_submodels($row)
    {
        if(is_array($row))
        {
            $row = $this->input->post($this->primary_key);
            $pk = $row;
        }
        else
        {
            $pk = $row;
        }

        if($this->fields)
        {
            foreach($this->fields as $field)
            {
                if(isset($field['type']) && $field['type'] == 'submodel' && isset($field['submodel']) && isset($field['submodel_field']))
                {
                    $this->insert_submodel($field['submodel'] . '_model', $field['submodel_field'], $pk, $field['field']);
                }

            }
        }
    }

    /**
     * @param $chave_tabela_atual_id
     * @param $submodel
     * @param $nome_post
     */
    public function insert_submodel($submodel, $nome_chave_id, $primary_key, $nome_post)
    {
        $this->load->model($submodel);

        $this->{"$submodel"}->delete_by(array(
            $this->primary_key => $primary_key
        ));

        $array = $this->input->post($nome_post);



        if($array && is_array($array))
        {
            foreach($array as $item)
            {

                if(is_array($item))
                {
                    $item[$this->primary_key] = $primary_key;
                    $dados = $this->{"$submodel"}->get_form_data(true, $item);
                }
                else
                {
                    $dados = array();
                    $dados[$nome_chave_id] = $item;
                    $dados[$this->primary_key] = $primary_key;


                }

                if($dados)
                {

                    $this->{"$submodel"}->insert($dados, true);
                }
            }

        }

    }


}
