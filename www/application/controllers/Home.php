<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Home
 */
class Home extends Public_Controller
{

    public function index()
    {
        $this->load->model("Plano_model","plano");

        $data = array();

        $data['planos'] = $this->plano->get_all();

        $this->template
            ->set("pagina_titulo", "A sua referência do artesanato")
            ->set("meta_descricao", "Tudo sobre Artesanato, Patchwork, Quilt, Cartonagem e Costura com o Estação Aquarela")
            ->set("meta_palavras_chave", "blog,artesanato,patchwork,quilt,cartonagem,costura,estacao,aquarela")
            ->load("templates/padrao/base", "site/home/index", $data);
    }
}
