<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Class Request
 * Requisições "Gerais"
 */
class ApiTest extends Test_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /**
     * Roda relatório de teste
     */
    public function run_report()
    {
        //Testa inserção de usuário
        $usuario_id = $this->test_usuario_insert();
        $this->unit->run($usuario_id, 'is_int', "Inserção de Usuário");

        //Testa a inserção dos telefones de Usuário
        $this->unit->run($this->test_usuario_telefone_insert($usuario_id), 'is_int', "Inserção de Telefone de Usuário");

        //Testa a inserção do Pagamento
        $pagamento_id = $this->test_usuario_pagamento($usuario_id);
        $this->unit->run($pagamento_id, 'is_int', "Inserção de Pagamento do Usuário");

        //Testa a inserção cartão NÃO AUTORIZADO
        $this->unit->run($cartao_nao_autorizado = $this->test_usuario_pagamento_cartao($pagamento_id, false), 'is_int', "Inserção de Cartão Não autorizado para Pagamento");

        //Testa processamento pagamento recusado
        $this->unit->run($this->processa_pagamento($cartao_nao_autorizado), "pagamento_recusado", "Verifica se pagamento foi RECUSADO.");

        //Testa a inserção cartão NÃO AUTORIZADO
        $this->unit->run($cartao_autorizado = $this->test_usuario_pagamento_cartao($pagamento_id, true), 'is_int', "Inserção de Cartão Autorizado para Pagamento");

        //Testa processamento pagamento recusado
        $this->unit->run($this->processa_pagamento($cartao_autorizado), "pagamento_aprovado", "Verifica se pagamento foi APROVADO.");


        echo "<h1>Relatório de testes:</h1>";
        echo $this->unit->report();
    }


    /**
     * Testa inserção de usuário
     */
    private function test_usuario_insert()
    {
        $this->load->model("Usuario_model","usuario");

        return $this->usuario->insert(array(
            'plano_id' => 3,
            'usuario_status_id' => 1,
            'nome' => "Teste",
            'sobrenome' => "Teste",
            'localizacao_id' => 5420,
            'cpf' => "12345678909",
            'email' => "teste@teste.com",
            'end_cep' => "09360160",
            'end_bairro' => "Teste",
            'end_logradouro' => "Rua teste",
            'end_numero' => "1234",
        ));
    }

    /**
     * Testa inserção de telefone do usuário
     */
    private function test_usuario_telefone_insert($usuario_id)
    {
        $this->load->model("Usuario_telefone_model","usuario_telefone");

        return $this->usuario_telefone->insert(array(
            'usuario_id' => $usuario_id,
            'telefone' => "11972855395",
        ));
    }

    /**
     * Testa inserção de telefone do usuário
     */
    private function test_usuario_pagamento($usuario_id)
    {
        $this->load->model("Pagamento_model","pagamento");

        return $this->pagamento->insert(array(
            'usuario_id' => $usuario_id,
            'status' => "aguardando",
        ), true);
    }

    /**
     * Testa inserção de telefone do usuário
     */
    private function test_usuario_pagamento_cartao($pagamento_id, $num_sucesso = false)
    {
        $this->load->model("Pagamento_cartao_model","pagamento_cartao");

        return $this->pagamento_cartao->insert(array(
            'pagamento_id' => $pagamento_id,
            'pagamento_cartao_status_id' => 1,
            'numero' => $num_sucesso ? "5555666677778884" : "1234123412341234",
            'nome' => "Teste de nome",
            'expira_em' => "2020-10-01",
            'cvc' => "123",
        ));
    }


    /**
     * Testa inserção de telefone do usuário
     */
    private function processa_pagamento($pagamento_cartao_id)
    {
        $this->load->model("Pagamento_cartao_model","pagamento_cartao");

        $this->pagamento_cartao->processa($pagamento_cartao_id);

        $pc = $this->pagamento_cartao
            ->with_foreign()
            ->get($pagamento_cartao_id);

        return $pc['pagamento_cartao_status_slug'];
    }
    //
}