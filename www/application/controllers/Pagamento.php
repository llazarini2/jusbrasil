<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Pagamento
 */
class Pagamento extends Public_Controller
{

    /**
     * Sucesso ao efetuar pagamento
     * @param $pagamento_id
     */
    public function sucesso($pagamento_cartao_id = 0)
    {
        $this->load->model("Pagamento_model","pagamento");
        $this->load->model("Pagamento_cartao_model","pagamento_cartao");
        $this->load->model("Usuario_model","usuario");

        $usuario_habilitado_id = $this->session->userdata("usuario_habilitado");

        $pagamento_cartao = $this->pagamento_cartao->get($pagamento_cartao_id);

        $pagamento = $this->pagamento->get_by(array(
            'pagamento_id' => $pagamento_cartao['pagamento_id'],
            'usuario_id' => $usuario_habilitado_id,
        ));


        if(!$pagamento )
        {
            redirect(base_url("Planos/contratar"));
        }

        $data = array();

        $this->template
            ->load("templates/padrao/base", "site/{$this->get_class()}/{$this->get_method()}", $data);

    }

    /**
     * Aviso de pagamento já efetuado
     */
    public function pagamento_ja_efetuado()
    {
        $this->template
            ->load("templates/padrao/base", "site/{$this->get_class()}/{$this->get_method()}", array());
    }

    /**
     * Efetua o pagamento
     * @param $pagamento_id
     */
    public function efetuar_pagamento($pagamento_id = 0)
    {
        $this->load->model("Pagamento_model","pagamento");
        $this->load->model("Usuario_model","usuario");
        $this->load->model("Usuario_telefone_model","usuario_telefone");

        $usuario_habilitado_id = $this->session->userdata("usuario_habilitado");

        $pagamento = $this->pagamento->get_by(array(
            'pagamento_id' => $pagamento_id,
            'usuario_id' => $usuario_habilitado_id,
        ));

        $data = array();


        if(!$pagamento )
        {
            redirect(base_url("Planos/contratar"));
        }
        else if ($pagamento['status'] == 'pago')
        {
            redirect(base_url("Pagamento/pagamento_ja_efetuado"));
        }

        //Resgata usuário
        $data['usuario'] = $this->usuario
            ->with_foreign()
            ->get($pagamento['usuario_id']);

        //Resgata telefones
        $data['telefones'] = $this->usuario_telefone
            ->get_many_by(array(
                'usuario_id' => $pagamento['usuario_id']
            ));


        $this->template
            ->css(app_assets_url("plugins/card.css","core"))
            ->js(app_assets_url("plugins/jquery.card.js","core"))
            ->load("templates/padrao/base", "site/{$this->get_class()}/{$this->get_method()}", $data);

    }
}
