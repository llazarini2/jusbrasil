<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Class Pagamento
 */
class Pagamento extends Rest_Controller
{
    function __construct()
    {
        parent::__construct();
    }


    /**
     * Acompanhar pagamento
     * @param int $pagamento_cartao_id
     */
    public function acompanhar_pagamento($pagamento_cartao_id = 0){

        $this->load->model("Pagamento_cartao_model","pagamento_cartao");

        $response = new Response();

        //Resgata cartão
        $pagamento_cartao = $this->pagamento_cartao
            ->with_foreign()
            ->get_by(array(
                'pagamento_cartao.pagamento_cartao_id' => $pagamento_cartao_id,
                'pagamento_cartao.pagamento_id' => $this->session->userdata("pagamento_id"),
            ));

        //Se existir cartão pagamento
        if($pagamento_cartao)
        {
            $response->setDados(array(
                'status' => $pagamento_cartao['pagamento_cartao_status_descricao'],
                'slug' => $pagamento_cartao['pagamento_cartao_status_slug'],
            ));

            $response->setStatus(true);
        }

        $response->getJSON();
    }

    /**
     * Efetua processamento dos pagamentos
     *  Atenção: Normalmente esta função ficaria rodando através do CRON, porém para fins de teste e demonstração,
     *           este método foi colocado aqui.
     */
    public function cron()
    {
        //Carrega biblioteca
        $this->load->library("encryption");

        //Carrega models
        $this->load->model("Pagamento_cartao_model","pagamento_cartao");
        $this->load->model("Pagamento_cartao_status_model","pagamento_cartao_status");

        //Busca status pendente
        $status_pendente = $this->pagamento_cartao_status->get_status("aguardando_processamento");

        //Busca pagamentos pendentes
        $pagamentos_pendentes = $this->pagamento_cartao->get_many_by(array(
            'pagamento_cartao_status_id' => $status_pendente['pagamento_cartao_status_id']
        ));

        if($pagamentos_pendentes )
        {
            //Processa cartão
            foreach($pagamentos_pendentes as $pagamento)
            {
                //Processa pagamento do cartão
                $this->pagamento_cartao->processa($pagamento['pagamento_cartao_id']);

            }
        }

    }
}