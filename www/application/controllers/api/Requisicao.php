<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Class Request
 * Requisições "Gerais"
 */
class Requisicao extends Rest_Controller
{
    protected $primeiro_acesso = false;

    function __construct()
    {
        parent::__construct();
    }


    /**
     * Retorna um registro
     * @param null $model
     * @param $id
     * @param string $directory
     */
    public function get($model = null, $id, $directory = '')
    {
        //Carrega data
        $data = array();
        $data['message'] = "Não foi possível resgatar os registros";
        $data['status'] = true;

        if($directory)
            $directory = $directory . '/';
        $model = ucfirst(strtolower($model));
        if(file_exists(APPPATH."models/{$directory}{$model}_model.php") )
        {
            //Carrega model necessário
            $this->load->model("{$directory}{$model}_model", 'current_model');

            //Retorna tudo
            $data['data'] = $this->current_model
                ->get($id);

            $data['total_rows'] = 1;
        }
        else
        {
            //Seta como status false
            $data['status'] = false;
        }

        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    /**
     * Retorna todos os dados
     * @param null $model
     * @param null $campo_id
     * @param null $campo_nome
     */
    public function get_all($model = null)
    {
        $this->load->library("Response");

        $response = new Response();

        //Converte para lower
        $model = ucfirst(strtolower($model));


        //Verifica se model existe
        if(file_exists(APPPATH."models/{$model}_model.php") )
        {
            //Carrega model necessário
            $this->load->model("{$model}_model", 'current_model');

            $per_page = $this->input->get_post("pageSize");
            $offset = $this->input->get_post("skip");
            $sort = $this->input->get_post("sort");
            $filters = $this->input->get_post("filter");
            $com_arquivos = $this->input->get_post("com_arquivos");
            $com_arquivos_slug = $this->input->get_post("com_arquivos_slug");

            //Retorna tudo
            $data = $this->current_model;

            if($per_page || $offset)
            {
                $data->limit($per_page, $offset);
            }

            if($sort)
            {
                foreach($sort as $ordem)
                {
                    $data->order_by($ordem['field'], $ordem['dir']);
                }
            }

            if($filters)
            {
                foreach($filters['filters'] as $filtro)
                {

                    $strategy = "where";

                    if(isset($filtro['strategy']))
                        $strategy = $filtro['strategy'];

                    if(is_array($filtro['value']))
                        $filtro['value'] = implode(",",$filtro['value']);

                    $operador = app_get_mysql_operator(issetor($filtro['operator'], "="));

                    $data->{$strategy}($filtro['field'] , $operador['operator'], $operador['before']. $filtro['value'] . $operador['after']);

                }
            }



            $data = $data
                ->with_foreign()
                ->get_all();


            //Se for com arquivos
            if($com_arquivos)
            {
                $this->arquivo->get_many_by(array(
                    'tabela' => $model,

                ));



            }


            $total = $this->current_model;

            $total->reset_joins();

            if($filters)
            {
                foreach($filters['filters'] as $filtro)
                {

                    $strategy = "where";

                    if(isset($filtro['strategy']))
                        $strategy = $filtro['strategy'];

                    if(is_array($filtro['value']))
                        $filtro['value'] = implode(",",$filtro['value']);

                    $operador = app_get_mysql_operator(issetor($filtro['operator'], "="));

                    $total->{$strategy}($filtro['field'] , $operador['operator'], $operador['before']. $filtro['value'] . $operador['after']);

                }
            }

            if(isset($total->grid_filter_ini) && $total->grid_filter_ini)
            {
                foreach ($total->grid_filter_ini as $index => $filtro)
                {
                    $ini_field = (isset($filtro['field'])) ? $filtro['field'] : null;
                    $ini_operator = (isset($filtro['operator'])) ? $filtro['operator'] : '=';
                    $ini_value = (isset($filtro['value'])) ? $filtro['value'] : null;
                    if($ini_field && $ini_value)
                    {
                        $total->where($ini_field, $ini_operator, $ini_value);
                    }
                }
            }

            $total = $total->with_foreign()->getTotal();
        }


        $response->setDados($data ? $data : array());
        $response->setStatus(true);
        $response->setTotal($total ? $total : 0);


        $response->getJSON();

    }


    /**
     *
     * @param string $path
     * @param null $model
     * @param string $group
     */
    public function add($model = null, $group = "default")
    {
        $response = new Response();

        $model = ucfirst(strtolower($model));
        if(file_exists(APPPATH."models/{$model}_model.php") )
        {
            $this->load->model("{$model}_model", "current_model");

            //Caso post
            if($_POST)
            {
                //Verifica se é válido
                if($this->current_model->validate_form($group))
                {
                    //Verifica se foi inserido
                    if($insert = $this->current_model->insert_form())
                    {
                        $response->setMessage("O registro foi inserido com sucesso.");
                        $response->setStatus(true);
                    }
                    else
                    {
                        $response->setMessage("Não foi possível inserir o registro. Por favor, entre em contato com o administrador.");
                        $response->setStatus(false);
                    }

                    $response->setDados($insert);

                    $redirecionamento = $this->input->post("redirect");
                    $redirecionamento = str_replace("{id}", $insert, $redirecionamento);

                    $response->setRedirect($redirecionamento);
                }
                else
                {
                    $response->setStatus(false);
                    $response->setDados(validation_errors_array());

                }
            }
        }
        else
        {

        }

        echo $response->getJSON();
    }

    /**
     * Edita um formulário
     * @param string $path
     * @param null $model
     * @param string $group
     */
    public function edit($model = null, $group = "default")
    {
        $this->load->library("Response");


        $response = new Response();
        $model = ucfirst(strtolower($model));
        if(file_exists(APPPATH."models/{$model}_model.php") )
        {

            $this->load->model("{$model}_model", "current_model");

            //Caso post
            if($_POST)
            {

                //Verifica se é válido
                if($this->current_model->validate_form($group))
                {
                    $pk = $this->input->post($this->current_model->primary_key());

                    $response->setDados($pk);

                    //Verifica se foi inserido
                    if($this->current_model->update_form())
                    {
                        $response->setMessage("O registro foi atualizado com sucesso.");
                        $response->setStatus(true);
                    }
                    else
                    {
                        $response->setMessage("Não foi possível atualizar o registro. Por favor, entre em contato com o administrador.");
                        $response->setStatus(false);
                    }

                    $response->setRedirect($this->input->post("redirect"));
                }
                else
                {
                    $response->setStatus(false);
                    $response->setDados(validation_errors_array());

                }
            }
        }

        echo $response->getJSON();
    }

    /**
     * Delete via portal
     * @param null $model
     * @param int $id
     */
    public function delete($model = null, $id = 0)
    {
        $this->load->library("Response");
        $response = new Response();
        $model = ucfirst(strtolower($model));
        if(file_exists(APPPATH."models/{$model}_model.php") )
        {
            $this->load->model("{$model}_model", "current_model");

            $row = $this->current_model->get($id);

            if ($row)
            {

                if ($this->current_model->delete($id))
                {
                    $response->setMessage("O registro foi removido com sucesso.");
                    $response->setStatus(true);
                }
            }
            else
            {
                $response->setMessage("Não foi possível remover o registro.");
                $response->setStatus(false);
            }


        }

        $response->getJSON();
    }
}