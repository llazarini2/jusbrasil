<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Form_Validate
 */
class Form_Validate extends Rest_Controller
{
    protected $primeiro_acesso = false;

    /**
     * Realiza validação de formulário
     * @param $form
     */
    public function validate($form, $group = 'default')
    {

        $response = new Response();

        $form = ucfirst($form);


        //Se model existe
        if(!file_exists(APPPATH."models/{$form}_model.php"))
        {
            $response->setMessage("Este model não existe.");
        }
        else
        {
            //Carrega model
            $this->load->model("{$form}_model", "current_model");

            if($this->current_model->validate_form($group))
            {
                $response->setStatus(true);
            }

            $response->setMessage("Este formulário é inválido.");
            $response->setDados(validation_errors_array());

        }

        $response->getJSON();

    }
}
