<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Planos
 */
class Planos extends Public_Controller
{

    /**
     * Página de cadastro para contratar um plano
     * @param int $plano_id
     */
    public function contratar($plano_id = "")
    {
        $this->load->model("Plano_model","plano");

        $data = array();

        $data['planos'] = $this->plano->get_all();
        $data['plano_id'] = $plano_id;

        $this->template
            ->load("templates/padrao/base", "site/{$this->get_class()}/{$this->get_method()}", $data);
    }

}
