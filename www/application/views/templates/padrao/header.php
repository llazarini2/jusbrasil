<head>

    <title>JusBrasil - Teste para FullStack</title>


    <!-- Importação do CSS Base e Bootstrap -->
    <link rel="stylesheet" href="<?php echo app_assets_url("css/bootstrap.min.css","site") ?>">

    <!-- Inclusão do Toastr -->
    <link rel="stylesheet" href="<?php echo app_assets_url("plugins/toastr.min.css", "core") ?>">

    <link rel="stylesheet" href="<?php echo app_assets_url("css/core.css","site") ?>">


    <!-- Set das variáveis globais utilizadas nas bibliotecas -->
    <script>
        var base_url = "<?php echo base_url() ?>";
        var base_api = "<?php echo base_url("api/") ?>";
    </script>

    <!-- Inclusão do Jquery e bibliotecas auxiliares de binding (Sightglass, Rivets e Validation) -->
    <script src="<?php echo app_assets_url("js/jquery-3.3.1.min.js", "core") ?>"></script>
    <script src="<?php echo app_assets_url("js/sightglass.js", "core") ?>"></script>
    <script src="<?php echo app_assets_url("js/rivets.js", "core") ?>"></script>
    <script src="<?php echo app_assets_url("js/rivets_base.js", "core") ?>"></script>
    <script src="<?php echo app_assets_url("js/validation.js", "core") ?>"></script>

    <!-- Mask -->
    <script src="<?php echo app_assets_url("plugins/jquery.mask.min.js", "core") ?>"></script>
    <script src="<?php echo app_assets_url("js/mask.js", "core") ?>"></script>

    <!-- Toastr -->
    <script src="<?php echo app_assets_url("plugins/toastr.min.js", "core") ?>"></script>

</head>