


<div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
    <h1 class="display-4">Escolha o plano perfeito</h1>
    <br>
    <p class="lead">Contrate o plano perfeito para as suas necessidades.</p>
</div>


<div class="card-deck mb-3 text-center">

    <?php foreach($planos as $plano) { ?>

        <div class="card mb-4 box-shadow">
            <div class="card-header">
                <h4 class="my-0 font-weight-normal"><?php echo $plano['nome'] ?></h4>
            </div>
            <div class="card-body">
                <h1 class="card-title pricing-card-title">R$<?php echo $plano['valor'] ?> <small class="text-muted">/ mês</small></h1>
                <ul class="list-unstyled mt-3 mb-4">
                    <?php echo issetor($plano['descricao']) ?>
                </ul>
                <a href="<?php echo base_url("Planos/contratar/{$plano['plano_id']}")?>" class="btn btn-lg btn-block btn-outline-primary">Contratar</a>
            </div>
        </div>

    <?php } ?>

</div>
