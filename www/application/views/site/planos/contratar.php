


<div class="container">

    <div class="py-5 text-center">
        <h2>Contratar plano</h2>
        <p class="lead">Contrate o plano perfeito para as suas necessidades.</p>
    </div>

    <div class="row">

        <!-- Início do formulário-->
        <div class="col-md-8 mb-12">

            <h4 class="mb-3"><span class="num">1</span> Informe seus dados pessoais</h4>

            <form class="form" style="width: 100%" model_validate="usuario" validate="true">

                <!-- Chaves -->
                <input type="hidden" name="new_record" value="1">
                <input type="hidden" id="plano_id" name="plano_id" value="<?php echo issetor($plano_id, "") ?>">
                <input type="hidden" name="redirect" value="<?php echo base_url("Pagamento/efetuar_pagamento/{id}") ?>">

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="nome">Nome <span class="text-muted">*</span></label>
                        <input name="nome" type="text" class="form-control">
                    </div>

                    <div class="col-md-6 mb-3">
                        <label for="sobrenome">Sobrenome <span class="text-muted">*</span></label>
                        <input name="sobrenome" type="text" class="form-control">
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-12">
                        <label for="email">E-mail <span class="text-muted">*</span></label>
                        <input name="email" type="text" class="form-control" id="email" placeholder="voce@exemplo.com.br">
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-md-12">
                        <label for="cpf">CPF <span class="text-muted">*</span></label>
                        <input name="cpf" type="text" class="form-control mask-cpf" id="email" placeholder="">
                    </div>
                </div>

                <div class="row mb-3">

                    <div class="col-md-12 mb-3 mt-3">
                        <h4><span class="num">2</span> Cadastre seu endereço</h4>
                    </div>

                    <div class="col-md-6">
                        <label for="cep">CEP <span class="text-muted">*</span></label>
                        <input name="end_cep" type="text" class="form-control mask-cep" rv-value="cep" placeholder="">
                    </div>

                    <div class="col-md-6">
                        <button class="btn btn-outline-primary btn-in-line" type="button" rv-on-click="buscar_cep"><i class="fa fa-search"></i> Buscar CEP</button>
                    </div>
                </div>


                <div class="row mb-3">

                    <div class="col-md-2">
                        <label for="cpf">Estado <span class="text-muted">*</span></label>
                        <select name="end_estado" class="custom-select" id="estado" rv-on-change="mudou_estado" rv-value="estado">
                            <option rv-each-estado="estados" rv-value="estado.localizacao_id">{estado.descricao}</option>
                        </select>
                    </div>

                    <div class="col-md-6">
                        <label for="cpf">Cidade <span class="text-muted">*</span></label>
                        <select name="localizacao_id" class="custom-select" id="cidade" rv-value="cidade">
                            <option rv-each-cidade="cidades" rv-value="cidade.localizacao_id">{cidade.descricao}</option>
                        </select>
                    </div>

                    <div class="col-md-4">
                        <label for="cpf">Bairro <span class="text-muted">*</span></label>
                        <input name="end_bairro" rv-value="bairro" type="text" class="form-control" id="bairro" placeholder="">
                    </div>

                </div>

                <div class="row mb-3">

                    <div class="col-md-6">
                        <label for="cpf">Logradouro <span class="text-muted">*</span></label>
                        <input name="end_logradouro" rv-value="logradouro" type="text" class="form-control" id="endereco" placeholder="">
                    </div>

                    <div class="col-md-3">
                        <label for="cpf">Nº <span class="text-muted">*</span></label>
                        <input name="end_numero" type="number" class="form-control" id="numero" placeholder="">
                    </div>

                    <div class="col-md-3">
                        <label for="cpf">Complemento </label>
                        <input name="end_complemento" type="text" class="form-control" id="complemento" placeholder="">
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12 mt-3">
                        <h4><span class="num">3</span> Cadastre seu telefone de contato</h4>

                        <button type="button" rv-on-click="adicionar_telefone" class="btn btn-outline-primary float-right">Adicionar telefone</button>

                    </div>
                </div>

                <div class="row mb-2" rv-each-telefone="telefones">

                    <div class="col-md-6">
                        <label for="nome">Telefone Celular <span class="text-muted">*</span></label>
                        <input rv-name="index | concat 'telefones[' index '][telefone]'" type="text" class="form-control mask-telefone" rv-value="telefone.telefone">
                    </div>

                    <div class="col-md-6">
                        <button type="button" rv-on-click="remover_telefone" class="btn btn-outline-danger btn-in-line">Remover</button>
                    </div>

                </div>

            </form>

        </div>
        <!-- Fim do formulário -->

        <!-- Início dos Planos -->
        <div class="col-md-4 mb-12">

            <h4 class="mb-3"><span class="num">4</span> Escolha seu plano</h4>


            <ul class="list-group mb-3 mt-5 planos">

                <?php if($planos) foreach($planos as $plano) { ?>

                    <li class="plano list-group-item d-flex justify-content-between lh-condensed" plano_id="<?php echo $plano['plano_id'] ?>">
                        <div>
                            <h6 class="my-0"><?php echo $plano['nome'] ?></h6>
                            <small class="text-muted"></small>
                        </div>
                        <span class="text-muted">R$<?php echo app_format_currency($plano['valor']) ?></span>

                        <button class="btn btn-light" type="button" rv-on-click="escolher_plano" plano_id="<?php echo $plano['plano_id'] ?>">Contratar este</button>

                    </li>

                <?php } ?>

            </ul>

            <button rv-on-click="submeter_formulario" class="btn btn-primary btn-lg btn-block mt-5" type="button">Continuar para o Pagamento</button>




        </div>
        <!-- Fim dos Planos -->

    </div>

</div>
