
<!-- Página para efetuar o pagamento -->
<div class="container">

    <div class="py-5 text-center">
        <h2>Efetuar Pagamento</h2>
        <p class="lead">Efetue o pagamento do seu plano e comece a usar a plataforma.</p>
    </div>

    <div class="row">

        <div class="col-md-12">
            <h3><?php echo $usuario['plano_nome'] ?>  <span class="badge badge-primary">R$<?php echo app_format_currency($usuario['plano_valor']) ?> / mês</span></h3>
        </div>

        <!-- Início dos Planos -->
        <div class="col-md-8 mb-12">

            <div class="row">

                <div class="col-md-6 mb-12">

                    <h4 class="mt-5 mb-4">Informações do Usuário</h4>


                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>Nome:</b> <?php echo $usuario['nome'] ?> <?php echo $usuario['sobrenome'] ?>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>E-mail:</b> <?php echo $usuario['email'] ?>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>CPF:</b> <?php echo $usuario['cpf'] ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 mb-12">
                    <h4 class="mt-5 mb-4">Endereço</h4>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>CEP:</b> <?php echo $usuario['end_cep'] ?>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>Cidade:</b> <?php echo $usuario['localizacao_descricao'] ?>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>Endereço:</b> <?php echo $usuario['end_logradouro'] ?>, Nº<?php echo $usuario['end_numero'] ?> <?php echo issetor($usuario['end_complemento']) ?>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b>Bairro:</b> <?php echo $usuario['end_bairro'] ?>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-12">

                    <h4 class="mt-5 mb-4">Informações do Contato</h4>


                    <?php if ($telefones) foreach($telefones as $i => $telefone) { ?>

                    <div class="row mb-3">
                        <div class="col-md-12">
                            <b><?php echo $i+1 ?>º Telefone:</b> <?php echo $telefone['telefone'] ?>
                        </div>
                    </div>

                    <?php } ?>
                </div>
            </div>

        </div>


        <!-- Fim dos Planos -->

        <!-- Início do formulário-->
        <div class="col-md-4 mb-12" rv-hide="acompanhar_pagamento">

            <div class="row">
                <div class="col-md-12">
                    <h4 class="mt-5 mb-3">Efetuar Pagamento</h4>
                </div>
            </div>


            <div class="row mb-4 mt-4">
                <div class="col-md-12">

                    <div class="card-wrapper">
                    </div>

                </div>

            </div>

            <div class="row">

                <div class="col-md-12">

                    <form class="form" style="width: 100%" model_validate="pagamento_cartao" validate="true">

                        <input type="hidden" name="new_record" value="1">

                        <div class="row">
                            <div class="col-md-12 mb-3">
                                <label for="numero">Número do Cartão de Crédito</label>
                                <input class="form-control" type="text" name="numero" id="numero">
                            </div>

                            <div class="col-md-12 mb-3">
                                <label for="nome">Nome do Titular do Cartão</label>
                                <input class="form-control" type="text" name="nome" id="nome" value="<?php echo $usuario['nome'] ?> <?php echo $usuario['sobrenome'] ?>">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="expira_em">Expira em</label>
                                <input class="form-control"type="text" name="expira_em" id="expira_em"/>
                            </div>

                            <div class="col-md-6 mb-3">
                                <label for="cvc">CVC</label>
                                <input class="form-control" type="text" name="cvc" id="cvc"/>
                            </div>
                        </div>

                        <button rv-on-click="submeter_formulario" class="btn btn-primary btn-lg btn-block" type="button">Confirmar Pagamento</button>

                    </form>

                </div>
            </div>

        </div>
        <!-- Fim do formulário -->

        <div class="col-md-4 mb-12" rv-show="acompanhar_pagamento">

            <div class="row">
                <div class="col-md-12 text-center">

                    <h3 class="text-center mt-5 aviso-msg">
                        { status }
                    </h3>

                    <img rv-show="mostra_loading" src="<?php echo base_url("assets/site/images/loading.gif") ?>">


                    <div rv-show="pagamento_reprovado">
                        <button class="btn btn-primary mt-5" type="button" rv-on-click="novo_pagamento">Efetuar novo pagamento</button>
                    </div>

                </div>
            </div>

        </div>

    </div>

</div>



