<footer class="pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">© <?php echo date("Y") ?></small>
        </div>
        <div class="col-6 col-md">
            <h5>Teste para Jusbrasil</h5>

        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="text-muted" href="<?php echo base_url() ?>">Home</a></li>
                <li><a class="text-muted" href="<?php echo base_url("Planos/contratar") ?>">Contratar um plano</a></li>

            </ul>
        </div>
        <div class="col-6 col-md">
            <p>Desenvolvido por Leonardo Lazarini.</p>
        </div>
    </div>
</footer>