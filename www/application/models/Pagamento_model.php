<?php
/**
 * Class Pagamento_model
 */
class Pagamento_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "pagamento";
    protected $primary_key = "pagamento_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    //Campos
    public $fields = array(

        array(
            'field' => 'usuario_id',
            'label' => 'Usuário',
            'rules' => 'required',
            'groups' => 'default',
            'foreign' => 'usuario',
        ),
        array(
            'field' => 'status',
            'label' => 'Status de Pagamento',
            'rules' => 'required',
            'groups' => 'default',
        ),
    );

    /**
     * Insere um pagamento e relaciona-o com o usuário
     * @param $usuario_id
     * @return bool
     */
    public function insere_pagamento($usuario_id)
    {
        return $this->insert(array(
            'usuario_id' => $usuario_id,
        ), true);
    }

}

