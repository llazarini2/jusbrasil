<?php

/**
 * Class Usuario_model
 */
class Usuario_telefone_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "usuario_telefone";
    protected $primary_key = "usuario_telefone_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    //Campos
    public $fields = array(

        array(
            'field' => 'usuario_id',
            'label' => 'Usuário correspondente',
            'rules' => 'required',
            'groups' => 'default',
            'foreign' => 'usuario',
        ),
        array(
            'field' => 'telefone',
            'label' => 'Telefone',
            'rules' => 'required|retorna_numeros|validate_telefone',
            'groups' => 'default',
        ),

    );


}

