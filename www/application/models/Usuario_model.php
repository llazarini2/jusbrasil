<?php

/**
 * Class Usuario_model
 */
class Usuario_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "usuario";
    protected $primary_key = "usuario_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    /**
     * Campos da tabela para validação e inserção
     * @var array
     */
    public $fields = array(


        array(
            'field' => 'plano_id',
            'label' => 'plano',
            'rules' => 'required',
            'groups' => 'default',
            'foreign' => 'plano',
        ),

        /**
         * Validação dos dados básicos
         */
        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'sobrenome',
            'label' => 'Sobrenome',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'email',
            'label' => 'E-mail',
            'rules' => 'required|valid_email',
            'groups' => 'default',
        ),
        array(
            'field' => 'cpf',
            'label' => 'CPF',
            'rules' => 'retorna_numeros|required|validate_cpf',
            'groups' => 'default',
        ),

        /**
         * Validação do Endereço
         */
        array(
            'field' => 'end_cep',
            'label' => 'CEP',
            'rules' => 'retorna_numeros|required|validate_cep',
            'groups' => 'default',
        ),
        array(
            'field' => 'end_logradouro',
            'label' => 'Logradouro',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'end_numero',
            'label' => 'Número',
            'rules' => 'required|numeric',
            'groups' => 'default',
        ),
        array(
            'field' => 'end_complemento',
            'label' => 'Complemento',
            'rules' => '',
            'groups' => 'default',
        ),
        array(
            'field' => 'end_bairro',
            'label' => 'Bairro',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'localizacao_id',
            'label' => 'Cidade',
            'rules' => 'required',
            'groups' => 'default',
            'foreign' => 'localizacao',
        ),



    );

    /**
     * Validação do formulário e formulários dependentes
     * @param string $group
     * @return bool
     */
    public function validate_form($group = 'default')
    {
        //Carrega model dependente para validação
        $this->load->model("Usuario_telefone_model","usuario_telefone");

        //Valida este formulário e os formulários dependentes
        if(parent::validate_form($group))
        {
            $telefones = $this->input->post("telefones");

            if($telefones) foreach($telefones as $telefone)
            {
                $telefone['usuario_id'] = 1;
                return $this->usuario_telefone->validate($telefone);
            }
            else
            {
                return false;
            }

            return true;
        }

        return false;

    }


    /**
     * Insere um formulário
     * @return bool|void
     */
    public function insert_form()
    {
        //Carrega models
        $this->load->model("Usuario_telefone_model","usuario_telefone");
        $this->load->model("Pagamento_model","pagamento");

        //Inicia transação para evitar inconsistências
        $this->db->trans_start();

        if($usuario_id = parent::insert_form())
        {
            $telefones = $this->input->get_post("telefones");

            if($telefones) foreach($telefones as $telefone)
            {
                $telefone['usuario_id'] = $usuario_id;
                $this->usuario_telefone->insert($telefone, true);
            }

            //Insere e registra o Pagamento
            $pagamento_id = $this->pagamento->insere_pagamento($usuario_id);

            //Finaliza transação
            if($this->db->trans_commit())
            {
                $this->session->set_userdata("usuario_habilitado", $usuario_id);
                $this->session->set_userdata("pagamento_id", $pagamento_id);

                return $pagamento_id;
            }

        }

        return false;

    }
}

