<?php

/**
 * Class Pagamento_cartao_status_model
 */
class Pagamento_cartao_status_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "pagamento_cartao_status";
    protected $primary_key = "pagamento_cartao_status_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    //Campos
    public $fields = array(
        array(
            'field' => 'descricao',
            'label' => 'Descrição',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'slug',
            'label' => 'Slug',
            'rules' => 'required',
            'groups' => 'default',
        ),
    );

    /**
     * Retorna Status de Pagamento
     * @param $status
     * @return bool|mixed
     */
    public function get_status($status)
    {
        return $this->get_by(array(
            'slug' => $status
        ));
    }
}

