<?php

/**
 * Class Localizacao_model
 */
class Localizacao_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "localizacao";
    protected $primary_key = "localizacao_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    public $fields = array(

        array(
            'field' => 'tipo',
            'label' => 'Tipo',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'descricao',
            'label' => 'Descrição',
            'rules' => 'required',
            'groups' => 'default',
        ),
    );

}

