<?php

/**
 * Class Plano_model
 */
class Plano_model extends MY_Model
{
    //Dados da tabela e chave primária
    protected $_table = "plano";
    protected $primary_key = "plano_id";

    //Configurações
    protected $return_type = 'array';

    //Soft delete
    protected $soft_delete = TRUE;

    //Campos
    public $fields = array(

        array(
            'field' => 'nome',
            'label' => 'Nome',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'descricao',
            'label' => 'Descrição',
            'rules' => 'required',
            'groups' => 'default',
        ),
        array(
            'field' => 'valor',
            'label' => 'Valor',
            'rules' => 'required',
            'groups' => 'default',
        ),
    );

}

