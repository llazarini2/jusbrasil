# PHP Challenge - Leonardo Lazarini

Teste do Leonardo Lazarini para PHP FullStack na JusBrasil;

  - Framework PHP: MVC CodeIgniter com PHP 7.2;
  - Base de Dados: MySQL 5.7;
  - Framework CSS: Bootstrap 4;
  - Bibliotecas Javascript: Rivets JS, Validation para Codeigniter, Mask JS e JQuery;

# Como inicializar o Projeto

1. Foi utilizado o ambiênte de containers Docker para o build do projeto, portanto o primeiro passo é instalá-lo (caso ainda não esteja instalado). Para isto, basta fazer o download no site oficial de acordo com o seu sistema operacional (obs: No linux pode ser necessário instalar o Docker Compose separadamente): 
 https://www.docker.com/get-started

2. Após baixar e instalar o Docker e configurá-lo, você deverá baixar o projeto através do GIT:
```sh
    git clone https://llazarini2@bitbucket.org/llazarini2/jusbrasil.git
```
3. Após baixar o repositório, entre na pasta através do CMD (windows) ou bash (Linux), e rode o Docker Compose (obs: pode ser necessário o SUDO caso o sistema operacional seja Linux)
```sh
    cd /minha/pasta/projeto
    
    docker-compose up
```
4. Pronto! Agora basta acessar localhost através do browser.
![Imagem do projeto](http://laitsoft.com.br/img-1.png)

# Testes de Cartão de Crédito
Para testar um cartão de crédito com status APROVADO, utilize o seguinte número: 5555666677778884

Para os testes com cartão RECUSADO utilize qualquer outro número.

# Telas do Projeto
O projeto possui duas telas de entrada para começar o preenchimento:
1. A Home, onde é possível verificar todos os planos disponíveis e iniciar o preenchimento do formulário através do plano escolhido.
2. A tela Contratar, onde nenhum plano é selecionado no primeiro momento, porém posteriormente o usuário pode selecioná-lo.

# Base de Dados
1. A base de dados irá subir automaticamente através do Docker Compose, mas caso desejar subir a base manualmente, deverá importar através do arquivo "local_do_projeto/mysql-dump/mysql-dump.sql".
2. Schema da Base de dados (também se encontra o Modelo do MySQL-Workbench na pasta "local_do_projeto/database"):
3. O banco pode ser acessado através de um cliente MySQL através da seguinte conexão:
HOST: localhost
PORTA: 7334
USUÁRIO: root
SENHA:1q2w3e

![Testes do projeto](http://laitsoft.com.br/database.png)

# Testes unitários
O relatório de testes unitários estão disponíveis através da seguinte URL: http://localhost/tests/ApiTest/run_report, onde será possível visualizar todos os testes cruciais para funcionamento do projeto.

Além disto, para fins de teste, o CRON de processamento dos cartões de crédito foi no Javascript da tela de preenchimento do cartão (de tempos em tempos). Porém, em um ambiente real ele deveria ser colocado em uma fila de processamento.

![Testes do projeto](http://laitsoft.com.br/testes.png)

